var indexSectionsWithContent =
{
  0: "abcdefghijlmnopqrstuvwx",
  1: "abcdefghijmprstv",
  2: "gmpsv",
  3: "abcdefgijprstv",
  4: "abcdefghijlmnpqrstuvw",
  5: "abcdefghilorstuvw",
  6: "bceprst",
  7: "delpst",
  8: "abcdefghijlmnopqrstuvwx",
  9: "acdefghilmnoprstuv",
  10: "abcdefghilmnopqrstuvwx"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "properties",
  10: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Properties",
  10: "Macros"
};

