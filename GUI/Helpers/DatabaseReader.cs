﻿using GUI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;

namespace GUI.Helpers
{
    /// <summary>
    /// Database reader manager
    /// </summary>
    public class DatabaseReader
    {
        /// <summary>
        /// Inserts structure description to the database
        /// </summary>
        /// <param name="path">Path to the file</param>
        /// <param name="model">Structure model</param>
        public string Insert(string path, StructureGeneratorModel model)
        {

            var guid = Guid.NewGuid().ToString();
            var fullPath = Path.Combine(path, guid + ".json");
            File.WriteAllText(fullPath, JsonConvert.SerializeObject(model));

            return guid;
        }
        /// <summary>
        /// Gets structure description from the database
        /// </summary>
        /// <param name="path">Path of the file</param>
        /// <param name="id">Id of the structure</param>
        /// <returns>Structure description</returns>
        public StructureGeneratorModel Get(string path, string id)
        {
            var fullPath = Path.Combine(path, id + ".json");

            StructureGeneratorModel model = JsonConvert.DeserializeObject<StructureGeneratorModel>
                (File.ReadAllText(fullPath));
            return model;
        }
    }
}