var _styles_8cs =
[
    [ "Styles", "class_model_1_1_styles.html", "class_model_1_1_styles" ],
    [ "StructureSize", "_styles_8cs.html#adfb77cf8498aa8765a984d9e3318e998", [
      [ "Small", "_styles_8cs.html#adfb77cf8498aa8765a984d9e3318e998a2660064e68655415da2628c2ae2f7592", null ],
      [ "Normal", "_styles_8cs.html#adfb77cf8498aa8765a984d9e3318e998a960b44c579bc2f6818d2daaf9e4c16f0", null ],
      [ "Large", "_styles_8cs.html#adfb77cf8498aa8765a984d9e3318e998a3a69b34ce86dacb205936a8094f6c743", null ],
      [ "Huge", "_styles_8cs.html#adfb77cf8498aa8765a984d9e3318e998aa2ad65f28a717b0fd2be860a0d8e5c3e", null ]
    ] ]
];