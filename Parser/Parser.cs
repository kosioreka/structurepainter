﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Text;
using Model;

namespace Parser
{
    /// <summary>
    /// The custom error in parsing exception
    /// </summary>
    public class ErrorException : ApplicationException
    {
        public ErrorException(string msg) : base(msg)
        {
        }

    }
    /// <summary>
    /// Class used for parsing text into structure of ParserObjects
    /// </summary>
    public class Parser
    {
        public Parser(Dictionary<string, Bitmap> images)
        {
            if (images == null)
                Images = new Dictionary<string, Bitmap>();
            else
                Images = images;
        }

        /// <summary>
        /// Scanner used to scan the text
        /// </summary>
        protected Scanner Scanner
        {
            get; set;
        }

        /// <summary>
        /// Currently scanned token
        /// </summary>
        protected Tokens Token
        {
            get; set;
        }
        /// <summary>
        /// The root of the structures
        /// </summary>
        protected ParserObject Root
        {
            get; set;
        }
        /// <summary>
        /// Images for graphic objects
        /// </summary>
        protected Dictionary<string, Bitmap> Images
        {
            get; set;
        }
        /// <summary>
        /// Method changing text into structure of ParserObjects
        /// </summary>
        /// <param name="text">Text to parse</param>
        /// <returns>The root of the parsed structure</returns>
        public ParserObject ParseText(string text)
        {

            Scanner = new Scanner(new MemoryStream(Encoding.UTF8.GetBytes(text)));
            Token = (Tokens)Scanner.Scan();
            Root = Start();
            if(Root==null)
                throw new ErrorException("Podany opis struktury jest nieprawidłowy i nie można na jego podstawie wygenerować wizualizacji.");
            return Root;

        }
        /// <summary>
        /// Method creating ParserObject
        /// </summary>
        /// <param name="isGraph">Whether the object is inside graph definition</param>
        /// <returns>Parsed ParserObject</returns>
        private ParserObject Start(bool isGraph = false)
        {
            ParserObject po;
            switch (Token)
            {
                case Tokens.ArrayStart:
                    Match(Tokens.ArrayStart);
                    po = Array0();
                    Match(Tokens.ArrayEnd);
                    break;
                case Tokens.BlockStart:
                    Match(Tokens.BlockStart);
                    Match(Tokens.Sequence);
                    po = Image0();
                    Match(Tokens.BlockEnd);
                    break;
                case Tokens.Sequence:
                    po = Sequence();
                    Match(Tokens.Sequence);
                    break;
                case Tokens.Number:
                    po = Sequence();
                    Match(Tokens.Number);
                    break;
                case Tokens.QuoteStart:
                    Match(Tokens.QuoteStart);
                    po = Sequence();
                    Match(Tokens.QuoteEnd);
                    break;
                case Tokens.Graph:
                    Match(Tokens.Graph);
                    po = Graph(false);
                    break;
                case Tokens.Tree:
                    Match(Tokens.Tree);
                    po = Graph(true);
                    break;
                case Tokens.VectorStart:
                    Match(Tokens.VectorStart);
                    po = Vector();
                    break;
                case Tokens.Error:
                    Root = null;
                    throw new ErrorException($"Błąd składniowy - nieoczekiwany symbol {Scanner.CurrentSymbol} na pozycji {Scanner.CurrentPosition} po tekście {Scanner.UntilNowText}");
                default:
                    po = null;
                    break;
            }
            if (po != null && po.Next == null && CheckToken(isGraph))
            {
                while (Token == Tokens.NewLine)
                    Match(Tokens.NewLine);
                po.Next = Start(isGraph);
                if (po.Next != null)
                {
                    po.Next.Previous = po;
                }
            }
            return po;
        }
        /// <summary>
        /// Method checking whether after current token a new object can be started
        /// </summary>
        /// <param name="isGraph">Whether there is ongoing parsing of the graph</param>
        /// <returns>True if the new object can be started, otherwise false</returns>
        private bool CheckToken(bool isGraph)
        {
            HashSet<Tokens> tokens = new HashSet<Tokens> {Tokens.ArrayStart, Tokens.Backslash, Tokens.BlockStart,
                Tokens.Graph, Tokens.QuoteStart, Tokens.Tree, Tokens.Sequence, Tokens.VectorStart, Tokens.Number, Tokens.NewLine};
            return tokens.Contains(Token) && !(isGraph && Token == Tokens.NewLine);
        }

        /// <summary>
        /// Creates VectorObject
        /// </summary>
        /// <returns>Parsed VectorObject</returns>
        protected ParserObject Vector()
        {
            VectorObject obj = new VectorObject();
            if (Token == Tokens.VectorDot)
            {
                Match(Tokens.VectorDot);
                obj.IsStart = true;
            }
            obj.Number = MatchNumber();
            if (obj.IsStart)
            {
                if (Token == Tokens.Label)
                {
                    obj.Label = MatchLabel();
                }
            }
            else
            {
                Match(Tokens.VectorDot);
            }

            Match(Tokens.VectorEnd);
            obj.Object = Start();
            if (obj.Object != null && obj.Object.Next != null)
            {
                obj.Next = obj.Object.Next;
                obj.Object.Next = null;
            }
            return obj;
        }
        /// <summary>
        /// Creates GraphObject
        /// </summary>
        /// <param name="isTree">Whether the object is a tree</param>
        /// <returns>Parsed GraphObject</returns>
        protected ParserObject Graph(bool isTree)
        {
            GraphObject obj = new GraphObject();
            obj.IsTree = isTree;
            Scanner.ClearText();
            while (Token == Tokens.NewLine)
            {
                Match(Tokens.NewLine);
            }
            Match(Tokens.BlockStart);
            if (Token == Tokens.NewLine)
            {
                Match(Tokens.NewLine);
            }
            //vertices
            while (Token == Tokens.Number)
            {
                int num = MatchNumber();
                Match(Tokens.Label);
                //vertice definition start
                obj.AddVertice(num, Start(true));
                while (Token == Tokens.NewLine)
                {
                    Match(Tokens.NewLine);
                }
            }
            if (obj.VerticesNumber < 1)
            {
                throw new ErrorException($"Graf musi mieć przynajmniej jeden wierzchołek.");
            }
            if (Token == Tokens.GraphSeparator)
            {
                Match(Tokens.GraphSeparator);
                while (Token == Tokens.NewLine)
                {
                    Match(Tokens.NewLine);
                }
                //edges
                while (Token == Tokens.Number)
                {
                    int v1, v2;
                    string label = null;
                    bool directed;
                    v1 = MatchNumber();
                    if (Token == Tokens.Directed)
                    {
                        Match(Tokens.Directed);
                        directed = true;
                    }
                    else if (Token == Tokens.Undirected)
                    {
                        Match(Tokens.Undirected);
                        directed = false;
                    }
                    else
                    {
                        throw new ErrorException($"Oczekiwano krawędzi, otrzymano: {Scanner.Text}");
                    }
                    v2 = MatchNumber();
                    if (Token == Tokens.Label)
                    {
                        label = MatchLabel();
                    }
                    obj.AddEdge(v1, v2, directed, label);
                    while (Token == Tokens.NewLine)
                    {
                        Match(Tokens.NewLine);
                    }
                }
            }

            Match(Tokens.BlockEnd);
            return obj;
        }
        /// <summary>
        /// Creates ImageObject
        /// </summary>
        /// <returns>Parsed ImageObject</returns>
        protected ParserObject Image0()
        {
            ImageObject obj = new ImageObject();

            if (!Images.ContainsKey(Scanner.Text))
            {
                throw new ErrorException($"Brak obrazka o nazwie: {Scanner.Text}");
            }
            obj.ImageBitmap = Images[Scanner.Text];
            Scanner.ClearText();
            return obj;
        }
        /// <summary>
        /// Creates SequenceObject
        /// </summary>
        /// <returns>Parsed SequenceObject</returns>
        protected ParserObject Sequence()
        {
            SequenceObject obj = new SequenceObject(Scanner.Text);
            Scanner.ClearText();
            return obj;
        }
        /// <summary>
        /// Creates ArrayObject
        /// </summary>
        /// <returns>Parsed ArrayObject</returns>
        protected ParserObject Array0()
        {
            ArrayObject obj = new ArrayObject();
            MatchHash(obj, 0);
            int i = 1;
            while (Token == Tokens.ArraySeparator)
            {
                Match(Tokens.ArraySeparator);
                MatchHash(obj, i);
                i++;
            }
            return obj;
        }
        /// <summary>
        /// Checks whether the current array field is a hash and creates object accordingly
        /// </summary>
        /// <param name="obj">Array to check</param>
        /// <param name="index">Index in the array to check</param>
        protected void MatchHash(ArrayObject obj, int index)
        {
            if (Token == Tokens.HashStart)
            {
                Match(Tokens.HashStart);
                obj.IsHashTable = true;
                obj.HashObjects.Add(index, new HashWrapper(Start(), true));
                Match(Tokens.HashEnd);
                Match(Tokens.VectorEnd);
                obj.Add(Start());
            }
            else
            {
                obj.Add(Start());
                if (Token == Tokens.VectorEnd)
                {
                    Match(Tokens.VectorEnd);
                    obj.IsHashTable = true;
                    obj.HashObjects.Add(index, new HashWrapper(Start(), false));
                }
            }
        }
        /// <summary>
        /// Matches number
        /// </summary>
        /// <returns>Scanned number</returns>
        protected int MatchNumber()
        {
            Match(Tokens.Number);
            int num;
            if (!int.TryParse(Scanner.Text, out num))
            {
                throw new ErrorException($"Nieprawidłowy numer wierzchołka: {Scanner.Text}");
            }
            Scanner.ClearText();
            return num;
        }
        /// <summary>
        /// Matches label
        /// </summary>
        /// <returns>Scanned labes</returns>
        protected string MatchLabel()
        {
            Match(Tokens.Label);
            StringBuilder label = new StringBuilder(Scanner.Text);
            Scanner.ClearText();
            if (Token == Tokens.Sequence)
                Match(Tokens.Sequence);
            else
                Match(Tokens.Number);
            while (Token == Tokens.Sequence || Token == Tokens.Number)
            {
                label.Append(" ").Append(Scanner.Text);
                Scanner.ClearText();
                Match(Token);
            }
            return label.ToString();
        }
        /// <summary>
        /// Matches current token and scanns the new one
        /// </summary>
        /// <param name="expected">Expected token to match</param>
        protected void Match(Tokens expected)
        {
            if (Token != expected)
            {
                Root = null;
                throw new ErrorException($"Błąd składniowy - nieoczekiwany symbol {Scanner.CurrentSymbol} na pozycji {Scanner.CurrentPosition} po tekście {Scanner.UntilNowText}");
            }
            Token = (Tokens)Scanner.Scan();
        }

    }
}
