﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Model;
using Brushes = System.Windows.Media.Brushes;
using Point = System.Windows.Point;

namespace VisualizerProject
{
    /// <summary>
    /// Helper for Visualization
    /// </summary>
    public class VisualizerHelper
    {
        /// <summary>
        /// The style of generated visualization
        /// </summary>
        protected Styles Styles;
        public VisualizerHelper(Styles styles)
        {
            Styles = styles;
        }

        /// <summary>
        /// Creates single graph or tree node on canvas
        /// </summary>
        /// <param name="element">Added canvas element</param>
        /// <param name="obj">Tree node object if given, his points for edge attachment is changed</param>
        /// <returns>Canvas as node</returns>
        public Canvas SetObjectInNode(Canvas element, TreeNodeObject obj = null)
        {
            var width = element.Width + Styles.NodeMarginWidth;
            var height = element.Height + Styles.NodeMarginHeight;

            Canvas can = new Canvas()
            {
                Width = width,
                Height = height,
                Background = Brushes.Transparent
            };
            if (obj != null)
            {
                obj.SetUpPoint(width / 2, Styles.StrokeSize / 2);
                obj.SetDownPoint(width / 2, height);
            }

            Ellipse ellipse = new Ellipse()
            {
                Height = height,
                Width = width,
                Stroke = Styles.LineColor,
                StrokeThickness = Styles.StrokeSize,
            };
            AddToCanvas(can, element, Styles.NodeMarginWidth / 2,
                Styles.NodeMarginHeight / 2);
            AddToCanvas(can, ellipse, 0, 0);

            return can;
        }
        /// <summary>
        /// Creates Bezier curve based on the given points
        /// </summary>
        /// <param name="points">Points for Bezier curve</param>
        /// <returns>Edge as line</returns>
        public Path DrawBezierCurves(List<Point> points)
        {
            PathGeometry path = new PathGeometry();
            PathFigure pathFigure = new PathFigure();
            pathFigure.StartPoint = points[0];
            path.Figures.Add(pathFigure);

            points.RemoveAt(0);
            PolyBezierSegment curve = new PolyBezierSegment(points, true);
            pathFigure.Segments.Add(curve);

            Path p = new Path();
            p.Stroke = Styles.LineColor;
            p.Data = path;
            p.StrokeThickness = Styles.StrokeSize;
            return p;
        }
        /// <summary>
        /// Changes point of edge attachment to the node
        /// </summary>
        /// <param name="obj">Tree node object</param>
        /// <param name="deltaX">Shift to the right</param>
        /// <param name="deltaY">Shift to the bottom</param>
        private void TranslateEdge(ref TreeNodeObject obj, double deltaX, double deltaY)
        {
            obj.SetUpPoint(deltaX, deltaY);
            obj.SetDownPoint(deltaX, deltaY);
        }
        /// <summary>
        /// Implements combining two canvases for tree elements and translating edges connection points
        /// </summary>
        /// <param name="can">Canvas to add element to</param>
        /// <param name="element">Element to add to the canvas</param>
        /// <param name="obj">Tree node object</param>
        /// <param name="left">Left offset</param>
        /// <param name="top">Top offset</param>
        /// <param name="onlyParent"> 0 - translates points of both parent and it's children, 
        ///                           1 - translates only parent
        ///                           2 - translates only children </param>
        public void CombineNodeCanvases(Canvas can, UIElement element, ref TreeNodeObject obj,
            double left, double top, int onlyParent = 0)
        {
            AddToCanvas(can, element, left, top);
            if (onlyParent == 0 || onlyParent == 1)
            {
                TranslateEdge(ref obj, left, top);
            }
            if ((onlyParent == 0 || onlyParent == 2) && obj.Children != null)
            {
                foreach (var c in obj.Children)
                {
                    var cc = c;
                    TranslateEdge(ref cc, left, top);
                }
            }
        }
        /// <summary>
        /// Adds element to canvas at defined position
        /// </summary>
        /// <param name="can">Canvas to add element to</param>
        /// <param name="element">Element to add to the canvas</param>
        /// <param name="left">Left offset</param>
        /// <param name="top">Top offset</param>
        /// <param name="zIndex">Z index of added element</param>
        public void AddToCanvas(Canvas can, UIElement element, double left, double top, int zIndex = 0)
        {
            can.Children.Add(element);
            Canvas.SetLeft(element, left);
            Canvas.SetTop(element, top);
            Panel.SetZIndex(element, zIndex);
        }
        /// <summary>
        /// Creates standard line
        /// </summary>
        /// <returns>Line with standard settings</returns>
        public Line CreateLine(double second = 0, bool isHorizontal = false)
        {
            if (!isHorizontal)
                return CreateLine(0, 0, 0, second);

            return CreateLine(0, 0, second, 0);
        }
        /// <summary>
        /// Creates line with standard style and delivered points
        /// </summary>
        /// <param name="x1">X1</param>
        /// <param name="y1">Y1</param>
        /// <param name="x2">X2</param>
        /// <param name="y2">Y2</param>
        /// <returns>Created line</returns>
        public Line CreateLine(double x1, double y1, double x2, double y2)
        {
            Line line = new Line()
            {
                Stroke = Styles.LineColor,
                StrokeThickness = Styles.StrokeSize,
                X1 = x1,
                X2 = x2,
                Y1 = y1,
                Y2 = y2
            };
            return line;
        }
        /// <summary>
        /// Draws arrow for a graph edge. Two points specify the direction of the graph
        /// </summary>
        /// <param name="p1">Point where arrow starts</param>
        /// <param name="p2">Point that specifies direction of the arrow</param>
        /// <returns>Canvas with arrow drown on it</returns>
        public Canvas CreateGraphArrow(Point p1, Point p2)
        {
            Line line;
            double x1 = p1.X;
            double y1 = p1.Y;
            double x = x1 - p2.X;
            double y = y1 - p2.Y;
            double rotX3 = x * Math.Cos(Styles.ArrowAngle) - y * Math.Sin(Styles.ArrowAngle);
            double rotY3 = x * Math.Sin(Styles.ArrowAngle) + y * Math.Cos(Styles.ArrowAngle);
            double rotX4 = x * Math.Cos(-Styles.ArrowAngle) - y * Math.Sin(-Styles.ArrowAngle);
            double rotY4 = x * Math.Sin(-Styles.ArrowAngle) + y * Math.Cos(-Styles.ArrowAngle);

            double ratio3 = Styles.ArrowLength / Math.Sqrt(Math.Pow(rotX3, 2) + Math.Pow(rotY3, 2));
            double ratio4 = Styles.ArrowLength / Math.Sqrt(Math.Pow(rotX4, 2) + Math.Pow(rotY4, 2));

            double x3 = x1 - rotX3 * ratio3;
            double y3 = y1 - rotY3 * ratio3;
            double x4 = x1 - rotX4 * ratio4;
            double y4 = y1 - rotY4 * ratio4;

            Canvas can = new Canvas()
            {
                Width = Math.Max(x1, Math.Max(x3, x4)),
                Height = Math.Max(y1, Math.Max(y3, y4)),
                Background = Brushes.Transparent
            };

            line = CreateLine(x1, y1, x3, y3);
            AddToCanvas(can, line, 0, 0);

            line = CreateLine(x1, y1, x4, y4);
            AddToCanvas(can, line, 0, 0);

            return can;
        }
        /// <summary>
        /// Creates a line egde between two vertices in tree object
        /// </summary>
        /// <param name="can">Output canvas</param>
        /// <param name="obj">Parent tree node</param>
        public void AddTreeEdges(Canvas can, TreeNodeObject obj)
        {
            for (int i = 0; i < obj.Children.Count; ++i)
            {
                var x1 = obj.DownPoint.X;
                var y1 = obj.DownPoint.Y;
                var x2 = obj.Children[i].UpPoint.X;
                var y2 = obj.Children[i].UpPoint.Y;
                Line line = new Line()
                {
                    Stroke = Styles.LineColor,
                    StrokeThickness = Styles.StrokeSize,
                    X1 = x1,
                    Y1 = y1,
                    X2 = x2,
                    Y2 = y2
                };
                var lineLength = Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
                var lineLengthMargin = lineLength - Styles.Margin / 2;

                AddToCanvas(can, line, 0, 0);
                if (obj.Edges[i].IsDirected)
                {
                    AddToCanvas(can, CreateGraphArrow(new Point(x2, y2),
                        new Point(x1, y1)), 0, 0);
                }
                var label = obj.Edges[i].Label;
                if (!string.IsNullOrEmpty(label))
                {
                    var labelBlock = new TextBlock
                    {
                        Text = label,
                        TextWrapping = TextWrapping.Wrap,
                        FontFamily = Styles.Font,
                        FontSize = Styles.FontSize,
                        Foreground = new SolidColorBrush(Styles.FontColor),
                        Background = Brushes.Transparent,
                        MaxWidth = lineLengthMargin
                    };
                    labelBlock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
                    labelBlock.Arrange(new Rect(labelBlock.DesiredSize));
                    var angleR = Math.Asin((y2 - y1) / lineLength);
                    var angleD = angleR * 180 / Math.PI;
                    var rightRot = x1 <= x2 || Math.Abs(x1 - x2) < 1;
                    angleD = rightRot ? angleD : -angleD;
                    RotateTransform rotate = new RotateTransform(angleD);
                    labelBlock.RenderTransform = rotate;

                    if (rightRot)
                    {
                        if (Math.Abs(x1 - x2) < 1)
                        {
                            var deltaY = (y1 + y2 - labelBlock.ActualWidth) / 2;
                            AddToCanvas(can, labelBlock, x1 + labelBlock.ActualHeight, deltaY);
                        }
                        else
                        {
                            var deltaZ = (lineLength - labelBlock.ActualWidth) / 2 + Styles.Margin / 2;
                            var deltaX = deltaZ * Math.Cos(angleR);
                            var deltaY = deltaZ * Math.Sin(angleR);
                            var deltaA = labelBlock.ActualHeight * Math.Sin(angleR);
                            var deltaH = labelBlock.ActualHeight * Math.Cos(angleR);
                            AddToCanvas(can, labelBlock, deltaX + x1 + Math.Sqrt(labelBlock.ActualHeight / 18.4d) * Styles.Margin,
                                 y1 + deltaY - deltaH);
                        }
                    }
                    else
                    {
                        var deltaZ = (lineLength - labelBlock.ActualWidth) / 2 - Styles.Margin / 2;
                        var deltaX = deltaZ * Math.Cos(angleR);
                        var deltaY = deltaZ * Math.Sin(angleR);
                        var deltaH = labelBlock.ActualHeight * Math.Cos(angleR);
                        var deltaA  = labelBlock.ActualHeight * Math.Sin(angleR);
                        AddToCanvas(can, labelBlock, deltaX + x2 - deltaA ,//Math.Sqrt(labelBlock.ActualHeight / 18.4d) * Styles.Margin - 2,
                             y2 - deltaY - deltaH);// - 0.9 * labelBlock.ActualHeight + 2);
                    }

                }
            }
        }
        public void AddLabelToTreeEdge()
        {

        }

        /// <summary>
        /// Method to get the total (recursive) offset of canvas
        /// </summary>
        /// <param name="can">Canvas to get offset for</param>
        /// <param name="inArray">Whether the canvas is in the array</param>
        /// <returns>The offset as a point</returns>
        public Point GetVectorLocation(Canvas can, bool inArray = false)
        {
            Canvas temp;
            Point p;
            if ((inArray && Styles.VerticalArray) || (Styles.VerticalFlow && !inArray))
                p = new Point(0, can.Height / 2);
            else
                p = new Point(can.Width / 2, 0);
            temp = can;
            while (temp.Parent != null)
            {
                p.X += Canvas.GetLeft(temp);
                p.Y += Canvas.GetTop(temp);
                temp = (Canvas)temp.Parent;
            }

            return p;
        }

        /// <summary>
        /// Draws hash arrow
        /// </summary>
        /// <param name="can">Canvas to draw on</param>
        /// <param name="start">Start point of the arrow</param>
        /// <param name="end">End point of the arrow</param>
        /// <param name="length">Horizontal line length from the start</param>
        /// <returns>Visualization with added hash arrow</returns>
        public Canvas DrawHash(Canvas can, Point start, Point end, double length)
        {
            double top = 0, left = 0;

            double x = Math.Cos(Styles.ArrowAngle) * Styles.ArrowLength;
            double y = Math.Sin(Styles.ArrowAngle) * Styles.ArrowLength;
            Line line;

            line = CreateLine(length + Styles.StrokeSize / 2, true);
            AddToCanvas(can, line, start.X, start.Y);
            line = CreateLine(end.X - start.X - length - Styles.StrokeSize / 2, true);
            AddToCanvas(can, line, start.X + length - Styles.StrokeSize / 2, end.Y);

            line = CreateLine(Math.Abs(start.Y - end.Y));
            AddToCanvas(can, line, start.X + length, Math.Min(start.Y, end.Y));

            //arrow
            line = CreateLine(0, x, y, 0);
            AddToCanvas(can, line, end.X - x - Styles.StrokeSize, end.Y);

            line = CreateLine(0, 0, x, y);
            AddToCanvas(can, line, end.X - x - Styles.StrokeSize, end.Y - y);

            return can;
        }
        public void ThrowDictionaryException(int number)
        {
            throw new ArgumentException($"Niewłaściwa definicja wektora o numerze {number}");
        }
    }

}
