﻿using Model;
using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace VisualizerProject
{
    /// <summary>
    /// Imports graphviz library
    /// </summary>
    public class Graphviz
    {
        /// <summary>
        /// gvc dll
        /// </summary>
        private const string LIB_GVC = "gvc.dll";
        /// <summary>
        /// cgraph dll
        /// </summary>
        private const string LIB_GRAPH = "cgraph.dll";
        /// <summary>
        /// Success field
        /// </summary>
        private const int SUCCESS = 0;
        /// <summary>
        /// Filename with rendered graph
        /// </summary>
        private const string FileName = "output1.dot";

        public Graphviz() { }

        /// <summary>
        ///  Creates a new Graphviz context.
        /// </summary>
        /// <returns>Graphviz context</returns>
        [DllImport(LIB_GVC, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr gvContext();
        /// <summary>
        /// Releases a context's resources.
        /// </summary>
        /// <param name="gvc">context</param>
        /// <returns>Success information</returns>
        [DllImport(LIB_GVC, CallingConvention = CallingConvention.Cdecl)]
        private static extern int gvFreeContext(IntPtr gvc);
        /// <summary>
        /// Reads a graph from a string.
        /// </summary>
        /// <param name="data">graph data</param>
        /// <returns>Pointer to rendered graph</returns>
        [DllImport(LIB_GRAPH, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr agmemread(string data);
        /// <summary>
        /// Releases the resources used by a graph.
        /// </summary>
        /// <param name="g">Graph pointer</param>
        [DllImport(LIB_GRAPH, CallingConvention = CallingConvention.Cdecl)]
        private static extern void agclose(IntPtr g);
        /// <summary>
        /// Returns the pointer to the node if exists
        /// Creates new node if doesn't exist
        /// </summary>
        /// <param name="g">graph</param>
        /// <param name="node">node's name</param>
        /// <param name="add">specifies whether or not a node of the given name should be created if it does not already exist</param>
        /// <returns>Found or created node</returns>
        [DllImport(LIB_GRAPH, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr agnode(IntPtr g, string node, int add);
        /// <summary>
        /// Returns the pointer to the edge if exists
        /// </summary>
        /// <param name="g">graph</param>
        /// <param name="np1">first node</param>
        /// <param name="np2">second node</param>
        /// <param name="edge">edge</param>
        /// <param name="add">specifies whether or not an edge of the given name should be created if it does not already exist</param>
        /// <returns>Found or created edge</returns>
        [DllImport(LIB_GRAPH, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr agedge(IntPtr g, IntPtr np1, IntPtr np2, string edge, int add);
        /// <summary>
        /// Returns attribute information
        /// </summary>
        /// <param name="node"></param>
        /// <param name="attribute"></param>
        /// <returns>Attribute information</returns>
        [DllImport(LIB_GRAPH, CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr agget(IntPtr obj, string name);
        /// <summary>
        /// Applies a layout to a graph using the given engine.
        /// </summary>
        /// <param name="gvc">context</param>
        /// <param name="g">graph instance</param>
        /// <param name="engine">type of engine</param>
        /// <returns>success information</returns>
        [DllImport(LIB_GVC, CallingConvention = CallingConvention.Cdecl)]
        private static extern int gvLayout(IntPtr gvc, IntPtr g, string engine);
        /// <summary>
        /// Releases the resources used by a layout.
        /// </summary>
        /// <param name="gvc">context</param>
        /// <param name="g">graph instance</param>
        /// <returns>success information</returns>
        [DllImport(LIB_GVC, CallingConvention = CallingConvention.Cdecl)]
        private static extern int gvFreeLayout(IntPtr gvc, IntPtr g);
        /// <summary>
        /// Renders a graph to a file.
        /// </summary>
        /// <param name="gvc">context</param>
        /// <param name="g">graph instance</param>
        /// <param name="format">format of rendered graph</param>
        /// <param name="fileName">name of file to be created</param>
        /// <returns>success information</returns>
        [DllImport(LIB_GVC, CallingConvention = CallingConvention.Cdecl)]
        private static extern int gvRenderFilename(IntPtr gvc, IntPtr g,
              string format, string fileName);
        /// <summary>
        /// Renders a graph in memory.
        /// </summary>
        /// <param name="gvc">context</param>
        /// <param name="g">graph</param>
        /// <param name="format">format of rendered graph</param>
        /// <param name="result">pointer to the result in bytes</param>
        /// <param name="length">length of the result</param>
        /// <returns></returns>
        [DllImport(LIB_GVC, CallingConvention = CallingConvention.Cdecl)]
        private static extern int gvRenderData(IntPtr gvc, IntPtr g,
              string format, out IntPtr result, out int length);
        /// <summary>
        /// Reads the given input, renders graph and exports it to the description
        /// </summary>
        /// <param name="source">source dot string with graph</param>
        /// <param name="layout">layout of the graph to render</param>
        /// <param name="format">format of the picture to render graph into</param>
        /// <returns>rendered graph in dot format</returns>
        public string RenderGraphvizString(string source, string layout, string format)
        {
            IntPtr gvc = IntPtr.Zero;
            IntPtr g = IntPtr.Zero;
            try
            {
                // Create a Graphviz context 
                gvc = gvContext();
                if (gvc == IntPtr.Zero)
                    throw new Exception("Failed to create Graphviz context.");

                // Load the DOT data into a graph 
                g = agmemread(source);//source
                if (g == IntPtr.Zero)
                    throw new Exception("Failed to create graph from source. Check for syntax errors.");

                // Apply a layout 
                if (gvLayout(gvc, g, Styles.GraphLayout) != SUCCESS)
                    throw new Exception("Layout failed.");

                var appDomain = AppDomain.CurrentDomain;
                var basePath = appDomain.RelativeSearchPath ?? appDomain.BaseDirectory;
                var path = Path.Combine(basePath, FileName);
                Console.WriteLine(path);

                //Render the graph
                if (gvRenderFilename(gvc, g, layout, path) != SUCCESS)
                    throw new Exception("Render failed.");

                string text = File.ReadAllText(path);

                //IntPtr result = IntPtr.Zero;
                //int length;
                //if (gvRenderData(gvc, g, format, out result, out length) != SUCCESS)
                //    throw new Exception("Render failed.");

                //var image = RenderImage(result, length);
                //Bitmap bmp = new Bitmap(image);
                //bmp.Save("outputDotImg.png");

                return text;
            }
            finally
            {
                // Free up the resources 
                gvFreeLayout(gvc, g);
                agclose(g);
                gvFreeContext(gvc);
            }
        }
        /// <summary>
        /// Creates image from the pointer to byte array
        /// </summary>
        /// <param name="result">pointer to byte array</param>
        /// <param name="length">length of the byte array</param>
        /// <returns>Image of the graph</returns>
        private Image RenderImage(IntPtr result, int length)
        {

            //Create anagopenarray to hold the rendered graph
            byte[] bytes = new byte[length];
            //Copy the image from the IntPtr
            Marshal.Copy(result, bytes, 0, length);

            using (MemoryStream stream = new MemoryStream(bytes))
            {
                return Image.FromStream(stream);
            }
        }
        /// <summary>
        /// An attempt to read attributes of the rendered graph instance
        /// </summary>
        /// <param name="g">graph instance</param>
        public void GetGraph(IntPtr g)
        {
            IntPtr np0 = agnode(g, "0", 0);
            IntPtr np1 = agnode(g, "1", 0);
            IntPtr np2 = agnode(g, "2", 0);
            if (np1 == IntPtr.Zero)
                throw new Exception("Node not found.");

            IntPtr edge = agedge(g, np0, np1, null, 0);

            IntPtr ePos = agget(edge, "len");

            IntPtr pos = agget(np1, "pos");

            char[] output = new char[3];

            Marshal.Copy(pos, output, 0, 3);
        }
    }

}
