﻿using QuickGraph;
using QuickGraph.Graphviz;
using System;
using System.Collections.Generic;
using System.Linq;
using Model;
using QuickGraph.Graphviz.Dot;
using TEdge = QuickGraph.TaggedEdge<System.Tuple<int, QuickGraph.Graphviz.Dot.GraphvizSizeF>, string>;
using TVertex = System.Tuple<int, QuickGraph.Graphviz.Dot.GraphvizSizeF>;
using Point = System.Windows.Point;
using Size = System.Windows.Size;
using System.Text.RegularExpressions;

namespace VisualizerProject
{
    /// <summary>
    /// Helper for graph creation in friendly graphviz environment 
    /// </summary>
    public class GraphHelper
    {
        /// <summary>
        /// The style of generated visualization
        /// </summary>
        private Styles Styles;
        public GraphHelper(Styles styles)
        {
            Styles = styles;
        }
        /// <summary>
        /// Generates graph using graphviz library to get the placement of graph elements on a plane 
        /// </summary>
        /// <param name="graph">Parsed graph instance</param>
        public void GenerateGraph(GraphObject graph)
        {
            try
            {
                var graphvizString = GenerateDot(graph);
                var graphviz = new Graphviz();
                var outputString = graphviz.RenderGraphvizString(graphvizString, "dot", "png");
                if (outputString != "")
                {
                    UpdateGraphFromFile(graph, outputString);
                }
            }
            catch(Exception e)
            {
                throw new Exception("Graphviz failed" + e.Message);
            }
        }
        /// <summary>
        /// Generates dot file from given graph object
        /// </summary>
        /// <param name="graph">Parsed graph instance</param>
        /// <returns>Dot format string output</returns>
        private string GenerateDot(GraphObject graph)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            AdjacencyGraph<TVertex, TEdge> gGraph = CreateGraph(graph);
            GraphvizAlgorithm<TVertex, TEdge> algo = new GraphvizAlgorithm<TVertex, TEdge>(gGraph);
            algo.FormatEdge += delegate (object sender, FormatEdgeEventArgs<TVertex, TEdge> e)
            {
                e.EdgeFormatter.Label.Value = e.Edge.Tag + e.Edge.Tag.Substring(e.Edge.Tag.Length / 2);
            };
            algo.FormatVertex += delegate (object sender, FormatVertexEventArgs<TVertex> e)
            {
                e.VertexFormatter.FixedSize = true;
                e.VertexFormatter.Label = e.Vertex.Item1.ToString();
                e.VertexFormatter.Size = e.Vertex.Item2;
            };

            string output = algo.Generate();//= algo.Generate(new BitmapGeneratorDotEngine(), "ignored");
            return output;
        }
        /// <summary>
        /// Creates graph that is rendered to dot file
        /// </summary>
        /// <param name="graph">Parsed graph instance</param>
        /// <returns>Graph instance from QuickGraph library</returns>
        private AdjacencyGraph<TVertex, TEdge> CreateGraph(GraphObject graph)
        {
            try
            {
                const int conv = 96;
                AdjacencyGraph<TVertex, TEdge> gGraph = new AdjacencyGraph<TVertex, TEdge>();
                Dictionary<int, TVertex> gVertices = new Dictionary<int, TVertex>();
                foreach (var v in graph.Vertices)
                {
                    var vertex = new TVertex(v.Id, new GraphvizSizeF((float)v.Object.MinSize.Width / conv, (float)v.Object.MinSize.Height / conv));
                    gVertices.Add(v.Id, vertex);
                    gGraph.AddVertex(vertex);
                }
                int i = 0;
                Edge e;
                while ((e = graph.GetEdge(i++)) != null)
                {
                    var edge = new TEdge(gVertices[e.V1], gVertices[e.V2], e.Label != null ? e.Label : String.Empty);
                    gGraph.AddEdge(edge);
                }
                return gGraph;

            }
            catch (Exception e)
            {
                throw e;          
            }
        }
        /// <summary>
        /// Reads positions for graph elements
        /// </summary>
        /// <param name="graph">Parsed graph instance to be updated</param>
        /// <param name="text">Rended dot text with graph information</param>
        private void UpdateGraphFromFile(GraphObject graph, string text)
        {
            var bbString = text.Substring(text.IndexOf("bb=") + 4);
            bbString = bbString.Substring(0, bbString.IndexOf('"'));
            var bbPos = bbString.Split(',');
            double shiftW = -double.Parse(bbPos[0]);
            double shiftH = -double.Parse(bbPos[1]);
            double picHeight = double.Parse(bbPos[3]) + shiftH;
            graph.MinSize = new Size(double.Parse(bbPos[2]) + shiftW, picHeight);
            //Vertices points
            for (int i = 0; i < graph.VerticesNumber; ++i)
            {
                var v = graph.Vertices[i];
                var index = text.IndexOf("\t" + i + "\t");
                var s = text.Substring(index);
                var attrSubstring = s.Substring(s.IndexOf('['), s.IndexOf(']') - s.IndexOf('['));
                var posSubstring = attrSubstring.Substring(attrSubstring.IndexOf("pos=") + 5);
                var value = posSubstring.Substring(0, posSubstring.IndexOf("\""));

                v.Point = GetPointFromString(value, picHeight, shiftW, shiftH);
            }
            //Edges points
            for (int i = 0; i < graph.EdgesNumber; ++i)
            {
                var e = graph.GetEdge(i);
                var index = text.IndexOf("\t" + graph.GetVertexIndex(e.V1) + " -> " + graph.GetVertexIndex(e.V2) + "\t");
                var s = text.Substring(index);
                var attrSubstring = s.Substring(s.IndexOf('['), s.IndexOf(']') - s.IndexOf('['));
                var posSubstring = attrSubstring.Substring(attrSubstring.IndexOf("pos=") + 5);
                var positions = posSubstring.Substring(0, posSubstring.IndexOf("\""));

                var pArr = positions.Split(' ').ToList();
                if (pArr[0].Contains('e'))
                {
                    var temp = pArr[0];
                    int sep = temp.IndexOf("e,") + 2;
                    temp = temp.Substring(sep);
                    pArr.RemoveAt(0);
                    pArr[pArr.Count - 1] = temp;
                }
                else if (pArr[0].Contains('s'))
                {
                    int sep = pArr[0].IndexOf("s,") + 2;
                    pArr[0] = pArr[0].Substring(sep);
                    pArr.RemoveAt(1);
                }
                for (int p = 0; p < pArr.Count; ++p)
                {
                    e.Points.Add(GetPointFromString(pArr[p], picHeight, shiftW, shiftH, true));
                }
                //Label point
                if (e.Label != null && e.Label != "")
                {
                    var lpSubstring = attrSubstring.Substring(attrSubstring.IndexOf("lp=") + 4);
                    var lpValue = lpSubstring.Substring(0, lpSubstring.IndexOf("\""));
                    e.LabelPoint = GetPointFromString(lpValue, picHeight, shiftW, shiftH);
                }
            }
        }
        /// <summary>
        /// Gets point from string that is in format of: x,picHeight-y
        /// </summary>
        /// <param name="value">Given string</param>
        /// <param name="picHeight">Height of the plane where the point is put</param>
        /// <param name="shiftW">Shift to right value</param>
        /// <param name="shiftH">Shift to bottom value</param>
        /// <param name="move">Whether to shift point using shift values</param>
        /// <returns>Point parsed from input string</returns>
        private Point GetPointFromString(string value, double picHeight, double shiftW, double shiftH, bool move = false)
        {
            double x, y;
            int comma = value.IndexOf(',');
            double.TryParse(Regex.Match(value.Substring(0, comma), @"\d+").Value, out x);
            double.TryParse(Regex.Match(value.Substring(comma + 1), @"\d+").Value, out y);
            if (move)
            {
                return new Point(x + Styles.Margin / 2 + shiftW, picHeight - y + Styles.Margin / 2 + shiftH);
            }
            return new Point(x+shiftW, picHeight - y+shiftH);
        }
    }
}
