var _style_model_8cs =
[
    [ "StyleModel", "class_g_u_i_1_1_models_1_1_style_model.html", "class_g_u_i_1_1_models_1_1_style_model" ],
    [ "Layout", "_style_model_8cs.html#aa0d3c659b4a1a1ec9dca653889faf4d7", [
      [ "Horizontal", "_style_model_8cs.html#aa0d3c659b4a1a1ec9dca653889faf4d7ac1b5fa03ecdb95d4a45dd1c40b02527f", null ],
      [ "Vertical", "_style_model_8cs.html#aa0d3c659b4a1a1ec9dca653889faf4d7a06ce2a25e5d12c166a36f654dbea6012", null ]
    ] ],
    [ "PolishArrayLayout", "_style_model_8cs.html#ab012e1ae16cc09f290eebcb45c85e311", [
      [ "Pozioma", "_style_model_8cs.html#ab012e1ae16cc09f290eebcb45c85e311aa0716a733f4fcaf2a1dbdf2a7f55e02d", null ],
      [ "Pionowa", "_style_model_8cs.html#ab012e1ae16cc09f290eebcb45c85e311af1118ae1676c2496c027c8200115be4a", null ]
    ] ],
    [ "PolishLayout", "_style_model_8cs.html#af91e08cab51e31a9b0fcafe347c8f5bb", [
      [ "Horyzontalny", "_style_model_8cs.html#af91e08cab51e31a9b0fcafe347c8f5bba3d5d2f59c22cefc8798ef275aaff3dac", null ],
      [ "Wertykalny", "_style_model_8cs.html#af91e08cab51e31a9b0fcafe347c8f5bba7eff10c4fb692ab43de3a8c7e8a14781", null ]
    ] ],
    [ "PolishSize", "_style_model_8cs.html#a55cce03a25e79080cf068a24a77ed45b", [
      [ "Mały", "_style_model_8cs.html#a55cce03a25e79080cf068a24a77ed45ba94fb7325f993c6b93c1679cca04ef056", null ],
      [ "Normalny", "_style_model_8cs.html#a55cce03a25e79080cf068a24a77ed45ba67cfc16eec709f64dbe71b7a976d99db", null ],
      [ "Duży", "_style_model_8cs.html#a55cce03a25e79080cf068a24a77ed45baa06303dd8701e8980c0ee62ad3f91590", null ],
      [ "Wielki", "_style_model_8cs.html#a55cce03a25e79080cf068a24a77ed45ba6f8485ce1f3ecff0529e149f4368e6f5", null ]
    ] ]
];