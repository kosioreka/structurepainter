﻿using System.Collections.Generic;

namespace Model
{
    /// <summary>
    /// Object containg array for visualization
    /// </summary>
    public class ArrayObject : ParserObject
    {
        /// <summary>
        /// This ParserObject elements e.g. array elements
        /// </summary>
        public List<ParserObject> Elements
        {
            get; protected set;
        }
        /// <summary>
        /// The objects arrows point to in hash table
        /// </summary>
        public Dictionary<int, HashWrapper> HashObjects;
        /// <summary>
        /// Indexer
        /// </summary>
        /// <param name="i">Index in Elements list</param>
        /// <returns>ParserObject object</returns>
        public ParserObject this[int i]
        {
            get
            {
                if (i >= Elements.Count)
                    return null;
                return Elements[i];
            }
            set
            {
                if (i <= Elements.Count)
                    Elements[i] = value;
            }
        }
        /// <summary>
        /// Add object to Elements list
        /// </summary>
        /// <param name="po">Object to add</param>
        public void Add(ParserObject po)
        {
            Elements.Add(po);
        }
        /// <summary>
        /// Number of objects in the list
        /// </summary>
        public int Count
        {
            get
            {
                if (Elements != null)
                    return Elements.Count;
                return 0;
            }
        }
        /// <summary>
        /// Whether it's a normal array or a hash one
        /// </summary>
        public bool IsHashTable { get; set; }
        public ArrayObject()
        {
            Elements = new List<ParserObject>();
            HashObjects = new Dictionary<int, HashWrapper>();
        }

    }
    /// <summary>
    /// The wrapper class for elements in hashtable
    /// </summary>
    public class HashWrapper
    {
        /// <summary>
        /// ParserObject the hash starts from / points to
        /// </summary>
        public ParserObject Object { get; set; }
        /// <summary>
        /// Whether the object is at the start of the hash arrow
        /// </summary>
        public bool IsStart { get; set; }
        public HashWrapper(ParserObject po, bool start)
        {
            Object = po;
            IsStart = start;
        }
    }
}
