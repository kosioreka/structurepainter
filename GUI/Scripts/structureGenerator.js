﻿var imageContainer = $('#image-structure');

var module = (function () {
    var Image;
    var Errors = [];
    cleanErrors = function () {
        Errors = [];
        $(".errors-container").html("");
    };
    var model = {
        Description,
        PictureUploads: [],
        Styles: {},
        DownloadFormat,
        setDescription: function () {
            model.Description = $("#Description").val()
        },
        setStyles: function () {
            model.Styles.Size = $("#Size").val();
            model.Styles.Font = $("#Font").val();
            model.Styles.FontColor = $("#FontColor_Name").val();
            model.Styles.LineColor = $("#LineColor_Name").val();
            model.Styles.Layout = $("#Layout").val();
            model.Styles.ArrayLayout = $("#ArrayLayout").val();
            model.Styles.OriginalImageSize = $("#OriginalImageSize").is(":checked");
        },
        setPictures: function () {
            var names = $('[id$="_Name"]');
            var i = 0;
            for (; i < names.length; ++i) {
                if (model.PictureUploads[i]) {
                    model.PictureUploads[i].Name = names[i].value;
                }
            }
        },
        setModel: function (generating) {
            if (generating) {
                this.setDescription();
                this.setPictures();
                this.setStyles();
            }
            this.DownloadFormat = $("#DownloadFormat").val()
        }
    }

    return {
        uploadPictures: function (picture) {
            $("#image-rows").append(picture.Html);
            $(".upload-row:hidden").show('normal');
            for (var i = 0; i < picture.Model.length; ++i) {
                model.PictureUploads.push(picture.Model[i]);
            }
            setTimeout(function () { $(".tick-img").fadeOut(1000); }, 2000);
        },
        addPictures: function (pictures) {
            for (var i = 0; i < pictures.length; ++i) {
                model.PictureUploads.push(pictures[i]);
            }
            $(".upload-row:hidden").show('normal');
            setTimeout(function () { $(".tick-img").fadeOut(100); }, 100);
        },
        setModelDescription: function (text) {
            if (text) {
                $("#Description").val(text);
            }
            model.setDescription();
        },
        getModel: function (generating) {
            model.setModel(generating);
            return model;
        },
        setImage: function (text) {
            Image = text;
            cleanErrors();
        },
        getImage: function () {
            return Image;
        },
        setErrors: function (errors) {
            Errors = [];
            var i = 0;
            var display = "";
            for (; i < errors.length; ++i) {
                Errors.push(errors[i]);
                //if (i == 0) {
                //display = "<li>" + errors[i].Message + "</li>";
                //} else {
                display += "<li>" + errors[i] + "</li>";
                //s }
            }
            popup.alert({ content: "<div class=\"error-container\">Znalezione błędy: </div><ul class=\"errors-list\"> " + display + "</ul>" });
        }
    };
})();

//overlay
(function () {
    var calculateHeight;
    calculateHeight = function () {
        var $content, contentHeight, finalHeight, windowHeight;
        $content = $('#overlay-content');
        contentHeight = parseInt($content.height()) + parseInt($content.css('margin-top')) + parseInt($content.css('margin-bottom'));
        windowHeight = $(window).height();
        finalHeight = windowHeight > contentHeight ? windowHeight : contentHeight;
        return finalHeight;
    };
    $(document).ready(function () {
        $(window).resize(function () {
            if ($(window).height() < 540 && $(window).width() > 600) {
                $('#overlay').addClass('short');
            } else {
                $('#overlay').removeClass('short');
            }
            return $('#overlay-background').height(calculateHeight());
        });
        $(window).trigger('resize');
        $('.popup-trigger').click(function () {
            return $('#overlay').addClass('open').find('.signup-form input:first').select();
        });
        $('#styles-btn').click(function () {
            $('#styles-content').removeClass('hidden');
            $('#pictures-upload-content').addClass('hidden');
        });
        $('#pictures-btn').click(function () {
            $('#styles-content').addClass('hidden');
            $('#pictures-upload-content').removeClass('hidden');
        });
        return $('#overlay-background,#overlay-close').click(function () {
            return $('#overlay').removeClass('open');
        });
    });
    var cookieValue = $.cookie("description");
    if (cookieValue && !$("#Description").val()) {
        module.setModelDescription(cookieValue);
    }
}.call(this));

// Styles //

// Setting style for fonts //
$("#Font option").each(function () {
    $(this).css("font-family", $(this).val());
});

$("#LineColor_Name option").each(function () {
    if ($(this).val() != '') {
        $(this).css('background-color', $(this).val());
    }
});
$("#FontColor_Name option").each(function () {
    if ($(this).val() != '') {
        $(this).css('background-color', $(this).val());
    }
});

$("#Description").on('input', function (e) {
    $.cookie("description", $(this).val());
});

if (Pictures) {
    module.addPictures(Pictures);
}
var clipboard = new Clipboard('.link-btn');

// ADD SLIDEDOWN ANIMATION TO DROPDOWN //
$('.dropdown').on('show.bs.dropdown', function (e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideDown();
});

// ADD SLIDEUP ANIMATION TO DROPDOWN //
$('.dropdown').on('hide.bs.dropdown', function (e) {
    $(this).find('.dropdown-menu').first().stop(true, true).slideUp();
});

// Ctrl-Enter pressed
$('body').keydown(function (e) {
    if (e.ctrlKey && e.keyCode == 13) {
        GenerateStructure();
    }
    if (e.ctrlKey && e.keyCode == 83) {
        DownloadStructure();
        return false;
    }
});

$("#generate-btn").popover(
    {
        content: "ctrl + enter",
        trigger: "hover",
        placement: "bottom"
    }
);
$("#download-btn").popover(
    {
        content: "ctrl + s",
        trigger: "hover",
        placement: "bottom"
    }
);
$(".fa-info-circle").popover(
    {
        content: "Obrazki można umieszczać w strukturze używając nawiasów klamrowych: {nazwa}",
        trigger: "hover",
        placement: "bottom"
    }
);

$('#file-upload').on('change', function (e) {
    var file = e.target.files[0];
    $(this).val("");
    if (file) {
        var reader = new FileReader();
        reader.readAsText(file, "UTF-8");
        reader.onload = function (evt) {
            module.setModelDescription(evt.target.result);
        }
        reader.onerror = function (evt) {
            alert("error reading file");
        }
    }
});

$("#picture-upload").on('change', function (e) {
    $(".header-row").removeClass("hidden");
    var files = e.target.files;
    if (files.length > 0) {
        $.LoadingOverlay("show");
        var data = new FormData();
        for (var i = 0; i < files.length; ++i) {
            if (files[i]) {
                data.append('file' + i, files[i]);
            }
        }
        $.ajax({
            type: "POST",
            url: '/StructureGenerator/UploadImageRow',
            contentType: false,
            processData: false,
            data: data,
            success: function (result) {
                module.uploadPictures(result);
            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                console.log(err);
                popup.alert({ content: "Dodanie zdjęcia nie powiodło się" });
            },
            complete: function () {
                $.LoadingOverlay("hide");
            }
        });
    } else {
        //alert("This browser doesn't support HTML5 file uploads!");
    }
    $(this).val("");
});

$("#generate-btn").on('click', function (e) {
    GenerateStructure();
});
function GenerateStructure() {
    var model = module.getModel(true);
    $.LoadingOverlay("show");
    $.ajax({
        url: '/StructureGenerator/Generate',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(model),
        success: function (result) {
            if (!result.isError) {
                imageContainer.html("<img id='theImg' src='data:image/png;base64," + result.value + "' />");
                module.setImage(result.value);

                $('#structure-content').removeClass('hidden');
                $.cookie("description", model.Description);
            } else {
                if (result.Error) {
                    errList = [];
                    errList.push(result.Error);
                    module.setErrors(errList);
                }
            }
        },
        error: function (e) {
        },
        complete: function () {
            $.LoadingOverlay("hide");
            $(".link-container").hide();
        }
    });
}

// Download section //
$("#download-btn").on('click', function () {
    DownloadStructure();
});

function DownloadStructure() {
    var model = module.getModel(false);
    $.LoadingOverlay("show");
    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/StructureGenerator/Download', true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function () {
        if (this.status === 200) {
            var filename = "";
            var disposition = xhr.getResponseHeader('Content-Disposition');
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            var type = xhr.getResponseHeader('Content-Type');

            var blob = new Blob([this.response], { type: type });
            if (typeof window.navigator.msSaveBlob !== 'undefined') {
                // IE workaround for "HTML7007: One or more blob URLs were revoked by closing the blob for which they were created. These URLs will no longer resolve as the data backing the URL has been freed."
                window.navigator.msSaveBlob(blob, filename);
            } else {
                var URL = window.URL || window.webkitURL;
                var downloadUrl = URL.createObjectURL(blob);

                if (filename) {
                    // use HTML5 a[download] attribute to specify filename
                    var a = document.createElement("a");
                    // safari doesn't support this yet
                    if (typeof a.download === 'undefined') {
                        window.location = downloadUrl;
                    } else {
                        a.href = downloadUrl;
                        a.download = filename;
                        document.body.appendChild(a);
                        a.click();
                    }
                } else {
                    window.location = downloadUrl;
                }

                setTimeout(function () { URL.revokeObjectURL(downloadUrl); }, 100); // cleanup
            }
        }
        $.LoadingOverlay("hide");
    };
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    xhr.send($.param(model));
}

$("#generate-link-btn").on('click', function () {
    var model = module.getModel(false);
    $.LoadingOverlay("show");
    $.ajax({
        url: '/StructureGenerator/GenerateLink',
        type: 'POST',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(model),
        success: function (result) {
            if (!result.isError) {
                $("#link").val("https://structurepainter.mini.pw.edu.pl/structuregenerator/get?id=" + result.Id);
                $(".link-container").show();
            } else {

            }
        },
        error: function (e) {
        },
        complete: function () {
            $.LoadingOverlay("hide");
        }
    });

});
