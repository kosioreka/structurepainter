var namespaces =
[
    [ "GUI", "namespace_g_u_i.html", "namespace_g_u_i" ],
    [ "Model", "namespace_model.html", null ],
    [ "ModuleTests", "namespace_module_tests.html", null ],
    [ "Parser", "namespace_parser.html", null ],
    [ "Shared", "namespace_shared.html", null ],
    [ "VisualizerProject", "namespace_visualizer_project.html", null ]
];