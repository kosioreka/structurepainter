﻿using System.Collections.Generic;
using System.Windows;

namespace Model
{
    /// <summary>
    /// Class representing structure object
    /// </summary>
    public abstract class ParserObject
    {
        /// <summary>
        /// Next object on the same level
        /// </summary>
        public ParserObject Next { get; set; }

        /// <summary>
        /// Previous object on the same level
        /// </summary>
        public ParserObject Previous
        {
            get; set;
        }

        /// <summary>
        /// Minimum size to draw it
        /// </summary>
        public Size MinSize
        {
            get; set;
        }
        protected ParserObject()
        {
            MinSize = new Size(0, 0);
        }
        
    }
}
