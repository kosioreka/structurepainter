﻿namespace Model
{
    /// <summary>
    /// Object containg text for visualization
    /// </summary>
    public class SequenceObject : ParserObject
    {
        /// <summary>
        /// Text to visualize
        /// </summary>
        public string Text
        {
            get; protected set;
        }
        public SequenceObject(string text)
        {
            Text = text;
        }
    }
}
