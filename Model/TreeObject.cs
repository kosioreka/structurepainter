﻿using System.Collections.Generic;
using System.Windows;

namespace Model
{
    /// <summary>
    /// Object containg tree for visualization
    /// </summary>
    public class TreeObject : ParserObject
    {
        /// <summary>
        /// Root of the tree
        /// </summary>
        public TreeNodeObject Root { get; set; }
        /// <summary>
        /// Specifies if the object is actually a tree
        /// </summary>
        public bool Generated { get; }
        public TreeObject(GraphObject graph)
        {
            //if (graph.IsTree)
            //{
                Generated = GenerateTreeFromGraph(graph);
            //}
        }
        /// <summary>
        /// Creates tree instance from given graph object
        /// </summary>
        /// <param name="graph">Graph</param>
        /// <returns>Boolean value if the graph was generated as a tree</returns>
        public bool GenerateTreeFromGraph(GraphObject graph)
        {
            Vertex root;
            if ((root = graph.GetRoot()) != null)
            {
                try
                {
                    Root = new TreeNodeObject(root, graph);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }
    }
    /// <summary>
    /// Object containg tree node for visualization
    /// </summary>
    public class TreeNodeObject : ParserObject
    {
        /// <summary>
        /// The node
        /// </summary>
        public Vertex Node { get; protected set; }
        /// <summary>
        /// Children of the node
        /// </summary>
        public List<TreeNodeObject> Children { get; set; }
        /// <summary>
        /// Children as edges objects. Order preserved as in Children property
        /// </summary>
        public List<Edge> Edges { get; protected set; }
        /// <summary>
        /// Egde attachment point to its parent
        /// </summary>
        public Point UpPoint { get; protected set; }
        /// <summary>
        /// Egde attachment point to its children
        /// </summary>
        public Point DownPoint { get; protected set; }

        public TreeNodeObject(Vertex vertex, GraphObject graph)//, List<Vertex> children)
        {
            Node = vertex;
            SetNode(graph);
            UpPoint = new Point();
            DownPoint = new Point();
        }
        /// <summary>
        /// Sets the node and its children
        /// </summary>
        /// <param name="graph">Graph to be rendered into tree</param>
        public void SetNode(GraphObject graph)
        {
            Children = new List<TreeNodeObject>();
            Edges = new List<Edge>();
            var children = graph.GetChildren(Node.Id);

            if (children?.Count > 0)
            {
                foreach (var child in children)
                {
                    var childNode = new TreeNodeObject(child, graph);
                    Children.Add(childNode);
                    Edges.Add(graph.GetEdge(Node.Id, child.Id));
                }
            }
            else
            {
                Children = null;
            }

        }
        /// <summary>
        /// Shifts UpPoint of the node
        /// </summary>
        /// <param name="x">Shift to the right</param>
        /// <param name="y">Shift to the bottom</param>
        public void SetUpPoint(double x, double y)
        {
            UpPoint = Point.Add(UpPoint, new Vector(x, y));
        }
        /// <summary>
        /// Shifts DowmPoint of the node
        /// </summary>
        /// <param name="x">Shift to the right</param>
        /// <param name="y">Shift to the bottom</param>
        public void SetDownPoint(double x, double y)
        {
            DownPoint = Point.Add(DownPoint, new Vector(x, y));
        }
    }
}
