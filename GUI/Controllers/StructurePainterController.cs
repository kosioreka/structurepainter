﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GUI.Controllers
{
    public class StructurePainterController : Controller
    {
        /// <summary>
        /// Main site
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// Examples site
        /// </summary>
        /// <returns></returns>
        public ActionResult Examples()
        {
            return View();
        }
        /// <summary>
        /// Generates site with Instruction file
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public FilePathResult Download(string fileName)
        {
            return new FilePathResult(string.Format(@"~\App_Data\{0}", fileName + ".pdf"), "application/pdf");
        }
    }
}