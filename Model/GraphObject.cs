﻿using System;
using System.Collections.Generic;
using System.Linq;
using Point = System.Windows.Point;

namespace Model
{
    /// <summary>
    /// Object containg graphs for visualization
    /// </summary>
    public class GraphObject : ParserObject
    {
        /// <summary>
        /// Whether this object is a tree
        /// </summary>
        public bool IsTree
        {
            get; set;
        }
        /// <summary>
        /// List of graph vertices
        /// </summary>
        public List<Vertex> Vertices
        {
            get; protected set;
        }
        /// <summary>
        /// List of graph edges
        /// </summary>
        protected List<Edge> Edges
        {
            get; set;
        }
        /// <summary>
        /// Adds vertice
        /// </summary>
        /// <param name="number">Vertice number</param>
        /// <param name="obj">Object that is in the vertice</param>
        public void AddVertice(int number, ParserObject obj)
        {
            if (!VerticesContains(number))
            {
                Vertices.Add(new Vertex(number, obj));
            }
            else
            {
                throw new Exception($"Wierzołek o numerze {number} już istnieje.");
            }
        }
        /// <summary>
        /// Number of vertices in the graph
        /// </summary>
        public int VerticesNumber => Vertices.Count;
        /// <summary>
        /// Number of edges in the graph
        /// </summary>
        public int EdgesNumber => Edges.Count;
        /// <summary>
        /// Gets the object in th specified vertice
        /// </summary>
        /// <param name="i">Vertex number</param>
        /// <returns>Object in the vertex</returns>
        public ParserObject GetVertexObject(int i)
        {
            if (VerticesContains(i))
            {
                return GetVertex(i).Object;
            }
            return null;
        }
        /// <summary>
        /// Gets the vertex
        /// </summary>
        /// <param name="id">Vertex id</param>
        /// <returns>The vertex</returns>
        public Vertex GetVertex(int id)
        {
            if (VerticesContains(id))
            {
                return Vertices.FirstOrDefault(v => v.Id == id);
            }
            return null;
        }
        /// <summary>
        /// Gets the vertex index in the list by its id
        /// </summary>
        /// <param name="id">Vertex id</param>
        /// <returns>Vertex index in the list</returns>
        public int GetVertexIndex(int id)
        {
            return Vertices.FindIndex(v => v.Id == id);
        }
        /// <summary>
        /// Checks whether the list contains a vertex with specified id
        /// </summary>
        /// <param name="id">Id of the vertex</param>
        /// <returns>True if there is such a vertex in the list</returns>
        protected bool VerticesContains(int id)
        {
            return Vertices.Find(v => v.Id == id) != null;
        }

        /// <summary>
        /// Adds edge to the list
        /// </summary>
        /// <param name="v1">Start vertex number</param>
        /// <param name="v2">End vertex number</param>
        /// <param name="isDirected">True if edge is directed</param>
        /// <param name="label">Edge label</param>
        public void AddEdge(int v1, int v2, bool isDirected, string label = null)
        {
            Edges.Add(new Edge(v1, v2, isDirected, label));
        }
        /// <summary>
        /// Checks whether the list contains specified edge
        /// </summary>
        /// <param name="edge">Edge to check</param>
        /// <returns>True if the list contains supplied edge</returns>
        public bool ContainsEdge(Edge edge)
        {
            return Edges.Contains(edge);
        }
        /// <summary>
        /// Returns edge of index i in the list
        /// </summary>
        /// <param name="i">The edge index</param>
        /// <returns>The edge under supplied index</returns>
        public Edge GetEdge(int i)
        {
            if (i < Edges.Count)
            {
                return Edges[i];
            }
            return null;
        }
        /// <summary>
        /// Returns edge of v1 and v2 vertices
        /// </summary>
        /// <param name="v1">First vertex number</param>
        /// <param name="v2">Second vertex number</param>
        /// <returns>The found edge</returns>
        public Edge GetEdge(int v1, int v2)
        {
            return Edges.Find(e => e.V1 == v1 && e.V2 == v2);
        }
        /// <summary>
        /// Returns all children of the given node. Applies only for trees. 
        /// Throws exeption if it is detected that a given graph is not a tree
        /// </summary>
        /// <param name="i">The node vertex</param>
        /// <returns>List of node childrens</returns>
        public List<Vertex> GetChildren(int i)
        {
            var cInt = Edges.FindAll(e => e.V1 == i).Select(c => c.V2);
            var children = Vertices.Where(v => cInt.Any(c => c == v.Id)).ToList();
            if (children == null)
                return null;
            if (children.Any(c => c.Visited == true))
            {
                throw new Exception();
            }
            children.ForEach(c => c.Visited = true);
            return children;
        }
        /// <summary>
        /// Returns the root of the tree if the graph is a tree
        /// </summary>
        /// <returns>The root of the tree</returns>
        public Vertex GetRoot()
        {
            var roots = Edges.Where(e1 => !Edges.Any(e2 => e2.V2 == e1.V1)).Select(e => e.V1).Distinct().ToList();
            if (roots.Count() != 1)
            {
                return null;
            }
            var root = GetVertex(roots[0]);
            root.Visited = true;
            return root;
        }
        public GraphObject()
        {
            Vertices = new List<Vertex>();
            Edges = new List<Edge>();
        }
    }
    /// <summary>
    /// Class representing a graph edge
    /// </summary>
    public class Edge : IEquatable<Edge>
    {
        /// <summary>
        /// Start vertex
        /// </summary>
        public int V1
        {
            get; set;
        }
        /// <summary>
        /// End vertex
        /// </summary>
        public int V2
        {
            get; set;
        }
        /// <summary>
        /// Whether the edge is directed
        /// </summary>
        public bool IsDirected
        {
            get; set;
        }
        /// <summary>
        /// Edge label
        /// </summary>
        public string Label
        {
            get; set;
        }
        /// <summary>
        /// The point of label visualization
        /// </summary>
        public Point LabelPoint
        {
            get; set;
        }
        /// <summary>
        /// Points of Bezier curve
        /// </summary>
        public List<Point> Points
        {
            get; set;
        }

        public Edge(int v1, int v2, bool isDirected, string label = null)
        {
            V1 = v1;
            V2 = v2;
            IsDirected = isDirected;
            Label = label;
            Points = new List<Point>();
        }
        /// <summary>
        /// Edge comparator
        /// </summary>
        /// <param name="edge">Edge to compare to the current one</param>
        /// <returns>True if the edges are equal</returns>
        public bool Equals(Edge edge)
        {
            return V1 == edge.V1 && V2 == edge.V2 && IsDirected == edge.IsDirected && Label == edge.Label;
        }
    }

    /// <summary>
    /// Class representing graph vertex
    /// </summary>
    public class Vertex
    {
        /// <summary>
        /// Vertex id
        /// </summary>
        public int Id
        {
            get; protected set;
        }
        /// <summary>
        /// The object in the vertex
        /// </summary>
        public ParserObject Object
        {
            get; protected set;
        }
        /// <summary>
        /// Point coordinates on the plane
        /// </summary>
        public Point Point
        {
            get; set;
        }
        /// <summary>
        /// Used for tree checking algorithm. Specifies if the vertex already was visited
        /// </summary>
        public bool Visited
        {
            get; set;
        }
        public Vertex(int id, ParserObject obj)
        {
            Id = id;
            Object = obj;
        }
    }

}
