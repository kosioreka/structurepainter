﻿namespace Model
{
    /// <summary>
    /// Object containg vectors for visualization
    /// </summary>
    public class VectorObject : ParserObject
    {
        /// <summary>
        /// True if this object is the start of the vector
        /// </summary>
        public bool IsStart { get; set; }
        /// <summary>
        /// Label for the vector
        /// </summary>
        public string Label { get; set; }
        /// <summary>
        /// Vector number/id
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// The object which is the start/end of the vector
        /// </summary>
        public ParserObject Object { get; set; }
    }
}
