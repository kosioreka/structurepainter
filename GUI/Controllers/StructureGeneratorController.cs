﻿using GUI.Helpers;
using GUI.Models;
using Model;
using Shared;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Mime;
using System.Web;
using System.Web.Mvc;

namespace GUI.Controllers
{
    public class StructureGeneratorController : Controller
    {
        private const string dbFile = "database.xml";

        /// <summary>
        /// Structure generater site
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            InitiateViewBag();
            var model = new StructureGeneratorModel();
            return View(model);
        }

        [HttpGet]
        public ActionResult Get(string id)
        {
            try
            {
                InitiateViewBag();
                var path = Server.MapPath("~/App_Data/");
                var dbReader = new DatabaseReader();
                var model = dbReader.Get(path, id);
                return View("Index", model);
            }
            catch (Exception e)
            {
                return RedirectToAction("Index");
            }
        }

        /// <summary>
        /// Generates structure
        /// </summary>
        /// <param name="model">Structure model</param>
        /// <returns>Json object with image structure or error information</returns>
        [HttpPost]
        public ActionResult Generate(StructureGeneratorModel model)
        {
            try
            {
                var helper = new GeneratorHelper();
                //var path = helper.GeneratePdfStructure(model);
                //var bytes = System.IO.File.ReadAllBytes(path);
                //var base64 = Convert.ToBase64String(bytes);
                var bmp = helper.GenerateImageStructure(model);
                var dbReader = new DatabaseReader();
                if (bmp != null)
                {

                    string base64String = ImageConverters.BitmapToBase64String(bmp);
                    return Json(new { value = base64String, isError = false });
                }
                else
                {
                    throw new Exception(ErrorType.Unknown.ToFriendlyString());
                }
            }
            catch (Exception e)
            {
                return Json(new { isError = true, Error = e.Message });
            }
        }
        /// <summary>
        /// Downloads structure to local user divice
        /// </summary>
        /// <param name="model">Structure model</param>
        /// <returns>File</returns>
        [HttpPost]
        public FileResult Download(StructureGeneratorModel model)
        {
            var helper = new GeneratorHelper();
            //var bmp = helper.GenerateStructure(model);
            var bmp = helper.GenerateImageStructure(model);
            var type = "image/png";
            var imgType = ImageFormat.Png;
            switch (model.DownloadFormat)
            {
                case DownloadType.PDF:
                    type = MediaTypeNames.Application.Pdf;
                    return File(helper.GeneratePdfStructure(model), type, "structure");
                //return File(ImageConverters.ImageToPdf(bmp), type, "structure");
                case DownloadType.JPG:
                    type = MediaTypeNames.Image.Jpeg;
                    imgType = ImageFormat.Jpeg;
                    break;
            }
            var img = ImageConverters.BitmapToByteArray(bmp, imgType);
            return File(img, type, "structure");
        }
        [HttpPost]
        public ActionResult GenerateLink(StructureGeneratorModel model)
        {
            try
            {
                var dbReader = new DatabaseReader();
                var path = Server.MapPath("~/App_Data/");
                string fullPath = Path.Combine(path, dbFile);
                var id = dbReader.Insert(path, model);

                return Json(new { isError = false, Id = id });
            }
            catch (Exception e)
            {
                return Json(new { isError = true, Error = e.Message });
            }
        }

        /// <summary>
        /// Uploads images files
        /// </summary>
        /// <returns>Images list model and rendered html view with image rows</returns>
        [HttpPost]
        public ActionResult UploadImageRow()
        {
            try
            {
                List<PictureUploadModel> images = new List<PictureUploadModel>(); ;

                for (var i = 0; i < Request.Files.Count; ++i)
                {
                    HttpPostedFileBase image = Request.Files[i];
                    var name = image.FileName;
                    name = name.Substring(0, name.LastIndexOf('.'));
                    var fileName = Path.GetFileName(image.FileName);
                    var bytesString = ImageConverters.FileToBase64String(image.InputStream);
                    //if (bytesString != null)
                    images.Add(new PictureUploadModel(name, image.FileName, bytesString));
                }

                string html = PartialView("~/Views/StructureGenerator/EditorTemplates/UploadRows.cshtml", images).RenderToString();
                return Json(new { Model = images, Html = html });
            }
            catch (Exception e)
            {
                return PartialView("~/Views/StructureGenerator/EditorTemplates/UploadRow.cshtml", new PictureUploadModel());
            }

        }
        #region ViewBag
        /// <summary>
        /// Initiates view bag
        /// </summary>
        private void InitiateViewBag()
        {
            ViewBag.Title = "Structure Generator";
            List<SelectListItem> liFonts = new List<SelectListItem>()
            {
                new SelectListItem{Value="Arial", Text = "Arial"},
                new SelectListItem{Value="Brush Script Std", Text = "Brush Script Std"},
                new SelectListItem{Value="Calibri", Text = "Calibri"},
                new SelectListItem{Value="Comic Sans MS", Text="Comic Sans MS"},
                new SelectListItem{Value="Liberation Serif", Text="Liberation Serif"},
                new SelectListItem{Value="Tahoma", Text = "Tahoma"},
                new SelectListItem{Value="Times New Roman", Text = "Times New Roman"},
                new SelectListItem{Value="Verdana", Text = "Verdana"},
            };
            ViewData["FontList"] = liFonts;

            List<SelectListItem> liColors = new List<SelectListItem>()
            {
                new SelectListItem{Value="Black", Text = "Czarny"},
                new SelectListItem{Value="Red", Text = "Czerwony"},
                new SelectListItem{Value="Blue", Text = "Niebieski"},
                new SelectListItem{Value="Green", Text = "Zielony"},
                new SelectListItem{Value="Gray", Text = "Szary"},
                new SelectListItem{Value="Yellow", Text = "Żółty"}
            };
            ViewData["FontColorList"] = liColors;
            ViewData["LineColorList"] = new List<SelectListItem>(liColors);
            ViewData["LayoutList"] = GetLayoutSelectList();
            ViewData["ArrayLayoutList"] = GetArrayLayoutSelectList();
            ViewData["SizeList"] = GetSizeSelectList();
        }
        /// <summary>
        /// Creates structure layouts for dropdown
        /// </summary>
        /// <returns>List of structure layouts</returns>
        private static List<SelectListItem> GetLayoutSelectList()
        {
            Array values = Enum.GetValues(typeof(Layout));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);
            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Value = Enum.GetName(typeof(Layout), i),
                    Text = Enum.GetName(typeof(PolishLayout), i)
                });
            }
            return items;
        }
        /// <summary>
        /// Creates array layouts for dropdown
        /// </summary>
        /// <returns>List of array layouts</returns>
        private static List<SelectListItem> GetArrayLayoutSelectList()
        {
            Array values = Enum.GetValues(typeof(Layout));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);
            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Value = Enum.GetName(typeof(Layout), i),
                    Text = Enum.GetName(typeof(PolishArrayLayout), i)
                });
            }
            return items;
        }
        /// <summary>
        /// Creates structure sizes for dropdown
        /// </summary>
        /// <returns>List of structure sizes</returns>
        private static List<SelectListItem> GetSizeSelectList()
        {
            Array values = Enum.GetValues(typeof(StructureSize));
            List<SelectListItem> items = new List<SelectListItem>(values.Length);
            foreach (var i in values)
            {
                items.Add(new SelectListItem
                {
                    Value = Enum.GetName(typeof(StructureSize), i),
                    Text = Enum.GetName(typeof(PolishSize), i)
                });
            }
            return items;
        }
    }
    #endregion
}