\select@language {polish}
\select@language {polish}
\select@language {english}
\select@language {polish}
\contentsline {chapter}{Wst\IeC {\k e}p}{9}{section*.1}
\contentsline {chapter}{\numberline {1}Analiza biznesowa}{11}{chapter.1}
\contentsline {section}{\numberline {1.1}Wymagania funkcjonalne}{11}{section.1.1}
\contentsline {section}{\numberline {1.2}User Stories}{12}{section.1.2}
\contentsline {section}{\numberline {1.3}Dost\IeC {\k e}pne aplikacje}{13}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Grafika rastrowa a wektorowa}{15}{subsection.1.3.1}
\contentsline {chapter}{\numberline {2}Analiza techniczna}{16}{chapter.2}
\contentsline {section}{\numberline {2.1}Model wytw\IeC {\'o}rczy}{16}{section.2.1}
\contentsline {section}{\numberline {2.2}Specyfikacja techniczna}{16}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}\IeC {\'S}rodowisko programistyczne}{16}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Wymagania niefunkcjonalne}{17}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Wykorzystywane biblioteki}{17}{section.2.3}
\contentsline {section}{\numberline {2.4}Wzorce projektowe}{17}{section.2.4}
\contentsline {chapter}{\numberline {3}Opis aplikacji}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Schemat komunikacji pomi\IeC {\k e}dzy modu\IeC {\l }ami}{21}{section.3.1}
\contentsline {section}{\numberline {3.2}Strona internetowa}{21}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Baza danych aplikacji}{21}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Widok przyk\IeC {\l }ad\IeC {\'o}w}{24}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Widok generatora}{24}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Instrukcja U\IeC {\.z}ytkownika}{27}{subsection.3.2.4}
\contentsline {subsection}{\numberline {3.2.5}B\IeC {\l }\IeC {\k e}dy}{29}{subsection.3.2.5}
\contentsline {section}{\numberline {3.3}Modu\IeC {\l } wizualizacji}{30}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Zasady wizualizacji}{30}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Obrazki}{31}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Tablice}{31}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Tablice haszuj\IeC {\k a}ce}{31}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Grafy}{32}{subsection.3.3.5}
\contentsline {subsection}{\numberline {3.3.6}Drzewa}{33}{subsection.3.3.6}
\contentsline {subsection}{\numberline {3.3.7}Wektory}{34}{subsection.3.3.7}
\contentsline {section}{\numberline {3.4}Parser}{34}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Definicja j\IeC {\k e}zyka do opisu struktur}{35}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}Przyk\IeC {\l }ady wykorzystania j\IeC {\k e}zyka}{36}{subsection.3.4.2}
\contentsline {section}{\numberline {3.5}Pozosta\IeC {\l }e modu\IeC {\l }y}{38}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Modu\IeC {\l } pomocniczy}{38}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Model}{38}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Modu\IeC {\l } test\IeC {\'o}w}{38}{subsection.3.5.3}
\contentsline {chapter}{\numberline {4}Testy}{40}{chapter.4}
\contentsline {section}{\numberline {4.1}Testy akceptacyjne}{40}{section.4.1}
\contentsline {section}{\numberline {4.2}Testy na u\IeC {\.z}ytkownikach}{42}{section.4.2}
\contentsline {chapter}{\numberline {5}Podsumowanie}{44}{chapter.5}
\contentsline {section}{\numberline {5.1}Mo\IeC {\.z}liwo\IeC {\'s}ci rozwoju}{44}{section.5.1}
\contentsline {chapter}{\numberline {A}Podzia\IeC {\l } pracy}{46}{appendix.A}
\contentsline {section}{\numberline {A.1}Podzia\IeC {\l } pracy przy dokumentacji}{46}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}Anna Kosiorek}{46}{subsection.A.1.1}
\contentsline {subsection}{\numberline {A.1.2}Patrycja Kukla}{46}{subsection.A.1.2}
\contentsline {section}{\numberline {A.2}Podzia\IeC {\l } pracy przy implementacji}{47}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}Anna Kosiorek}{47}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}Patrycja Kukla}{47}{subsection.A.2.2}
\contentsline {chapter}{\numberline {B}S\IeC {\l }ownik poj\IeC {\k e}\IeC {\'c}}{48}{appendix.B}
\contentsline {chapter}{\numberline {C}Przyk\IeC {\l }ady u\IeC {\.z}ycia j\IeC {\k e}zyka}{50}{appendix.C}
\contentsline {chapter}{\numberline {D}Testy}{55}{appendix.D}
\contentsline {chapter}{Bibliografia}{58}{section*.39}
\contentsline {chapter}{Spis rysunk\'ow}{59}{section*.40}
\contentsfinish 
