\select@language {polish}
\contentsline {section}{\numberline {1}Wst\IeC {\k e}p}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}S\IeC {\l }ownik poj\IeC {\k e}\IeC {\'c}}{3}{subsection.1.1}
\contentsline {section}{\numberline {2}Metodyka}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Model wytw\IeC {\'o}rczy}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}\IeC {\'S}rodowisko programistyczne}{3}{subsection.2.2}
\contentsline {section}{\numberline {3}Architektura}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Wzorce projektowe i biblioteki}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Struktura aplikacji}{4}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Modu\IeC {\l } parsera i modu\IeC {\l } wizualizacji}{4}{subsection.3.3}
\contentsline {subsubsection}{\numberline {3.3.1}Struktura Options}{4}{subsubsection.3.3.1}
\contentsline {subsubsection}{\numberline {3.3.2}ParserObject}{4}{subsubsection.3.3.2}
\contentsline {subsubsection}{\numberline {3.3.3}Parser}{6}{subsubsection.3.3.3}
\contentsline {subsection}{\numberline {3.4}Schemat komunikacji mi\IeC {\k e}dzy modu\IeC {\l }ami}{6}{subsection.3.4}
\contentsline {section}{\numberline {4}Serwer}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Zapis danych}{6}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Zapytania Http}{8}{subsection.4.2}
\contentsline {section}{\numberline {5}Interfejs u\IeC {\.z}ytkownika}{8}{section.5}
\contentsline {subsection}{\numberline {5.1}Widok g\IeC {\l }\IeC {\'o}wny}{8}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Widok poboczny}{10}{subsubsection.5.1.1}
\contentsline {section}{\numberline {6}Parser i j\IeC {\k e}zyk parsera}{11}{section.6}
\contentsline {subsection}{\numberline {6.1}Definicja j\IeC {\k e}zyka}{11}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Przyk\IeC {\l }ady zastosowania j\IeC {\k e}zyka}{12}{subsection.6.2}
