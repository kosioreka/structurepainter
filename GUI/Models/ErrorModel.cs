﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUI.Models
{
    /// <summary>
    /// Error types
    /// </summary>
    public enum ErrorType
    {
        PictureNotFound, SyntaxError, DuplicatedPicture, Unknown
    }
    /// <summary>
    /// Extension for errors
    /// </summary>
    public static class ErrorLevelExtensions
    {
        /// <summary>
        /// Gets String from error
        /// </summary>
        /// <param name="errType">Type of error</param>
        /// <param name="mssg">Additional message</param>
        /// <returns>Error friendly information</returns>
        public static string ToFriendlyString(this ErrorType errType, string mssg = "")
        {
            switch (errType)
            {
                case ErrorType.PictureNotFound:
                    return "Nieznaleziono obrazka: '" + mssg + "'";
                case ErrorType.SyntaxError:
                    return "Zły znak. " + mssg;
                case ErrorType.DuplicatedPicture:
                    return "Zdefiniowane dwa obrazki o tych samych nazwach: '" + mssg + "'. Nazwa musi być unikalna.";
                case ErrorType.Unknown:
                    return "Nieznany błąd w generacji. " + mssg;
                default:
                    return mssg;
            }
        }
    }
}