# README #

This project is a part of my Bachelor's thesis.

### What is this repository for? ###

* The subject of this project is the visualization of data structures based on specially designed
language. The software package is primarily intended for scientific purposes in order to simplify the creation of auxiliary drawings of data structures. It is an alternative to the commonly used for this purpose graphics software such as Paint, GIMP or Microsoft Visio. Its simplicity significantly accelerates and facilitates the preparation of teaching materials.
* The described functionality is available on the website with an intuitive and simple interface. Additionally, it is equipped with a manual and a description of the language with examples.

### What are the main components of the project? ###

* Parser - parses the user's input using top-down parsing approach
* Visualizer - draws data structers on a Canvas. Data stuctures are drawn using simple geometric properties and mathematical theorems
* Website - interaction with user

### What kind of algorithms were applied in the project? ###

* Checking if the given graph is a tree structure (acyclic graph and only one root)

### How do I get set up? ###

* MVC ASP.NET project
* Graphviz dlls libraries should be uploaded to the bin folder (http://www.graphviz.org/Download.php)
* The website may be on MiNI WUT (Warsaw University of Technology) server for a while (avaliable on the website: https://structurepainter.mini.pw.edu.pl/)