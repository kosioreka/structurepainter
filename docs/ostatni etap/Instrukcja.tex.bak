\documentclass[12pt,a4paper]{article}
%polski język
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{polski}
\usepackage[utf8]{inputenc}
\usepackage[polish]{babel}

\let\lll\undefined
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}
%grafika
\usepackage{graphicx}
%linki
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    linkcolor={black!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}
%listy
\usepackage{enumitem}
%kolory
\usepackage[usenames, dvipsnames]{color}
%tabele
\usepackage{array}
%tabela dobrej szerokości
\usepackage{tabularx}
%obrót obrazka
\usepackage{wrapfig}
\usepackage{lscape}
\usepackage{rotating}
\usepackage{epstopdf}
%domyślny podpis rysunków i tabel
\addto\captionspolish{\renewcommand{\figurename}{Ryc.}}
\addto\captionspolish{\renewcommand{\tablename}{Tab.}}
%tekst bez interpretowania poleceń latex
\usepackage{alltt}
%caption - kustomizacja podpisów obrazków
\usepackage{caption}

%\usepackage{changepage}
\usepackage{amsmath}
\usepackage{float}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{lastpage}
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{adjustbox}
\usepackage[colorinlistoftodos]{todonotes}
\setlength\extrarowheight{2pt}
\begin{document}
	\begin{titlepage}
		\title{\vspace{6 cm}\textbf{Structure Painter}\\
			\vspace{1 cm} {Instrukcja obsługi}\\
			\vspace{1 cm} \emph{Język oraz pakiet oprogramowania dla wizualizacji struktur danych}}
		
		\author{Anna Kosiorek \\ Patrycja Kukla}
		\maketitle
	\end{titlepage}

\newpage

	\section{O oprogramowaniu}
		\emph{Structure Painter} to aplikacja służąca do sprawnego generowania wizualizacji struktur danych. Jest ona dostępna jako strona internetowa pod adresem: \url{https://structurepainter.mini.pw.edu.pl} \\\\
		Pakiet oprogramowania jest przeznaczony do wykorzystywania w celach naukowych jako ułatwienie generowania pomocniczych rysunków struktur danych. Będzie alternatywą dla zwykle używanych w tym celu programów graficznych, takich jak np. Paint, CorelDraw czy Visio. Jego prosta obsługa ma za zadanie zdecydowanie przyspieszyć i ułatwić przygotowywanie materiałów dydaktycznych.\\\\
		Niniejszy dokument zawiera instrukcję obsługi przeznaczoną dla użytkownika.	
		\subsection{Funkcjonalności aplikacji}
		Użytkownik korzystając z programu ma możliwość wygenerowania struktur danych jako obrazek. W tym celu należy opisać wybrane do wizualizacji dane w specjalnie opracowanym, którego opis i przykłady użycia znajdują się na stronie internetowej \href{https://structurepainter.mini.pw.edu.pl/StructurePainter/Examples}{Przykłady}.\\\\
	Dokładne omówienie funkcjonalności oraz korzystania z aplikacji znajduje się w rozdziale odnoszącym się do \hyperref[sec:gen]{widoku generatora}.		
		
	\section{Strony}
		\subsection{Strona główna}
		Na stronie głównej użytkownik może zapoznać się z przeznaczeniem aplikacji oraz wybrać jej część, do której chce przejść. Wśród dostępnych opcji znajdują się:
		\begin{itemize}
			\item generator wizualizacji
			\item opis języka z przykładami
			\item instrukcja użytkownika
		\end{itemize}
		\subsection{Przykłady}
		Widok ten zawiera definicję języka wraz z przykładami jego wykorzystania do wizualizacji struktur.
		\subsection{Generator}
		\label{sec:gen}
		Widok udostępniający główną funkcjonalność oprogramowania tj. generowanie struktur. \\
		\begin{figure}[H]
			\begin{adjustbox}{width=1\textwidth}
				\centering
				\includegraphics[width=\linewidth]{main.png}
			\end{adjustbox}
			\caption{Widok generatora z ponumerowanymi elementami intefejsu}
		\end{figure}
	Opis interfejsu:
		\begin{enumerate}

		\item Przycisk służący do wgrania opisu struktury z pliku, którego pojawi się w widocznym okienku tekstowym. Opis można też wpisać ręcznie i zmienić w dowolnej chwili.
		\item Ustawienia wizualizacji: 
		\begin{itemize}
			\item Zachowaj oryginalny rozmiar obrazka - wybór tej opcji spowoduje, że wgrane grafiki zachowają swój rozmiar. Domyślnie są one skalowane, aby lepiej wyglądały po wizualizacji.
			\item Czcionka - wybór rodzaj czcionki
			\item Kolor czcionki - wybór koloru czcionki
			\item Kolor kontur - wybór koloru obramowania struktur
			\item Rozmiar - wybór wielkości generowanej struktury (marginesy, czcionka, maksymalne wymiary obrazków)
			\item Układ - wybór rozmieszczenia kolejnych struktur: horyzontalnie bądź wertykalnie
			\item Tablica - wybór układu tablicy: w pionie bądź w poziomie
		\end{itemize}
		\begin{figure}[H]
				\centering
				\includegraphics[width=0.7\linewidth]{style.png}
			\caption{Panel styli}
		\end{figure}
		
		\item Aby wgrać zdjęcia należy wybrać opcję \emph{Obrazki}. Po wgraniu wybranych zdjęć, ukażą się one w kolejnych wierszach. Dla każdego obrazka nadana jest nazwa, którą należy się posłużyć w momencie umieszczania obrazu w strukturze. Podaną nazwę można zmienić, jednak wobec innych zdjęć powinna być ona unikalna. W przeciwnym wypadku użytkownik przy próbie generacji otrzyma informację o błędzie.	
		
		\item Opcja \emph{Stwórz} oraz skrót klawiszowy \emph{ctrl} + \emph{enter} generuje obraz struktury.  Struktura zostanie zwizualizowana poniżej okienka tekstowego. W przypadku błędów generowania, użytkownik zostanie poinformowany.
		
		\item Wygenerowaną strukturkę można zapisać w następujących formatach: PDF, PNG oraz JPG. Po wyborze opcji \emph{Zapisz} obraz struktury zostanie ściągnięty w wybranym formacie do folderu na komputerze użytkownika.

		\end{enumerate}
		\subsubsection{Błędy generacji}
		Do najczęstszych błędów generowania należą:
		\begin{itemize}
			\item Błąd w definicji struktury - błąd składniowy. W przypadku próby generowania z błędym opisem ukaże się okienko z informacją o miejscu znalezionego błędu.
			\item Brak zdefiniowanego obrazka. Należy sprawdzić, czy nazwa obrazka, który będzie załączony do struktury się zgadza.
			\item Brak unikalnych nazw dla wgranych obrazków. Każde wgrane zdjęcie powinno mieć inną nazwę. Można ją zmienić w opcjach \emph{styli}.			
		\end{itemize}
		\begin{figure}[H]
			\begin{adjustbox}{width=1\textwidth}
				\centering
				\includegraphics{error1.png}
			\end{adjustbox}
			\caption{Błąd w definicji struktury \\Niepoprawny znak został wykryty na 17 pozycji}
		\end{figure}
	
	
\end{document}

