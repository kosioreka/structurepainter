﻿
using Model;
using System.Drawing;

namespace GUI.Models
{
    /// <summary>
    /// Style model
    /// </summary>
    public class StyleModel
    {
        /// <summary>
        /// Size of the structure
        /// </summary>
        public StructureSize Size { get; set; }
        /// <summary>
        /// Font type
        /// </summary>
        public string Font { get; set; }
        /// <summary>
        /// Color of the structure
        /// </summary>
        public Color LineColor { get; set; }
        /// <summary>
        /// Font color
        /// </summary>
        public Color FontColor { get; set; }
        /// <summary>
        /// Layout of the structure
        /// </summary>
        public Layout Layout { get; set; }
        /// <summary>
        /// Layout of array structure
        /// </summary>
        public Layout ArrayLayout { get; set; }
        /// <summary>
        /// Whether set original size of image 
        /// </summary>
        public bool OriginalImageSize { get; set; } = false;
    }
    /// <summary>
    /// Structure layout types in polish
    /// </summary>
    public enum PolishLayout { Horyzontalny, Wertykalny }
    /// <summary>
    /// Array layout types in polish
    /// </summary>
    public enum PolishArrayLayout { Pozioma, Pionowa }
    /// <summary>
    /// Structure layout types
    /// </summary>
    public enum Layout { Horizontal = PolishLayout.Horyzontalny, Vertical = PolishLayout.Wertykalny }
    /// <summary>
    /// Structure size types in polish
    /// </summary>
    public enum PolishSize { Mały = StructureSize.Small, Normalny = StructureSize.Normal,
        Duży = StructureSize.Large, Wielki = StructureSize.Huge }
}