﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Drawing;
using Model;
using Parser;

namespace ModuleTests
{
    [TestClass]
    public class ScanTests
    {
        #region sequence
        [TestMethod]
        public void EmptySequenceTest()
        {
            TestException("");
            //string text = "";
            //Parser.Parser p = new Parser.Parser(null);
            //ParserObject po = p.ParseText(text);
            //Assert.AreEqual(null, po);
        }

        [TestMethod]
        public void SimpleSequenceTest()
        {
            string text = "AlaMa2KotyIżadnegoPsa";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual(text, ((SequenceObject)po).Text);
        }
        [TestMethod]
        public void SimpleNumberAsSequenceTest()
        {
            string text = "213423";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual(text, ((SequenceObject)po).Text);
        }

        [TestMethod]
        public void PolishCharactersSequenceTest()
        {
            string text = "ęółśążźćńĄŚĆŹŻĘŃÓŁ";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual(text, ((SequenceObject)po).Text);
        }

        [TestMethod]
        public void BackslashSequenceTest()
        {
            string text = "\\[";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual("[", ((SequenceObject)po).Text);
        }
        #endregion
        #region array
        [TestMethod]
        public void EmptyArrayTest()
        {
            string text = "[]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(1, ((ArrayObject)po).Count);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
        }
        [TestMethod]
        public void EmptyMultipleElementsArrayTest()
        {
            string text = "[|||]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(4, ((ArrayObject)po).Count);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
        }
        [TestMethod]
        public void SimpleArrayTest()
        {
            string text = "[a]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(1, ((ArrayObject)po).Count);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
            Assert.AreEqual(typeof(SequenceObject), ((ArrayObject)po)[0].GetType());
            Assert.AreEqual("a", ((SequenceObject)((ArrayObject)po)[0]).Text);
        }

        [TestMethod]
        public void NestedArraysTest()
        {
            string text = "[[ala|ma]|kota|[a]|[ale | {ten10}]]";
            var images = new Dictionary<string, Bitmap>();
            Bitmap bmp = new Bitmap(10, 10);
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(4, ((ArrayObject)po).Count);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());

            Assert.AreEqual(2, ((ArrayObject)((ArrayObject)po)[0]).Count);
            Assert.AreEqual(typeof(ArrayObject), ((ArrayObject)po)[0].GetType());
            ArrayObject ao = ((ArrayObject) ((ArrayObject) po)[0]);
            Assert.AreEqual("ala", ((SequenceObject)ao[0]).Text);

            ao = ((ArrayObject) ((ArrayObject) po)[3]);
            Assert.AreEqual(2, ao.Count);
            Assert.AreEqual(typeof(ArrayObject), ao.GetType());
            Assert.AreEqual(typeof(ImageObject), ao[1].GetType());
            Assert.AreEqual(10, ((ImageObject)ao[1]).ImageBitmap.Width);
        }
        #endregion
        #region HashTable
        [TestMethod]
        public void SimpleHashTableTest()
        {
            string text = "[ala>ma]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
            ArrayObject ao = (ArrayObject) po;
            Assert.AreEqual(1, ao.Count);
            Assert.IsTrue(ao.IsHashTable);
            Assert.AreEqual(1, ao.HashObjects.Count);
            Assert.IsNull(ao.Elements[0].Next);
            Assert.IsNull(ao.HashObjects[0].Object.Next);
        }
        [TestMethod]
        public void SimpleWithNextHashTableTest()
        {
            string text = "[ala>ma kota| Mruczka]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
            ArrayObject ao = (ArrayObject)po;
            Assert.AreEqual(2, ao.Count);
            Assert.IsTrue(ao.IsHashTable);
            Assert.AreEqual(1, ao.HashObjects.Count);
            Assert.IsNull(ao.Elements[0].Next);
            Assert.IsNotNull(ao.HashObjects[0].Object.Next);
        }
        [TestMethod]
        public void SimpleMirrorHashTableTest()
        {
            string text = "[(ala)>ma kota]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
            ArrayObject ao = (ArrayObject)po;
            Assert.AreEqual(1, ao.Count);
            Assert.IsTrue(ao.IsHashTable);
            Assert.AreEqual(1, ao.HashObjects.Count);
            Assert.IsNotNull(ao.Elements[0].Next);
            Assert.IsNull(ao.HashObjects[0].Object.Next);
        }
        #endregion
        #region image
        [TestMethod]
        public void SimpleImageTest()
        {
            string text = "{ten10}";
            var images = new Dictionary<string, Bitmap>();
            Bitmap bmp = new Bitmap(10, 10);
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(ImageObject), po.GetType());
            Assert.AreEqual(10, ((ImageObject)po).ImageBitmap.Width);
        }
        #endregion
        #region multilevel
        [TestMethod]
        public void SimpleMultiTest()
        {
            string text = "ala ma";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreNotEqual(null, po.Next);
            Assert.AreNotEqual(null, po.Next.Previous);
            Assert.AreEqual(typeof(SequenceObject), po.Next.GetType());
            Assert.AreEqual(typeof(SequenceObject), po.Next.Previous.GetType());
        }
        [TestMethod]
        public void MultiArrayTest()
        {
            string text = "[][][]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
            Assert.AreNotEqual(null, po.Next);
            Assert.AreEqual(typeof(ArrayObject), po.Next.GetType());
            Assert.AreNotEqual(null, po.Next.Previous);
            Assert.AreEqual(typeof(ArrayObject), po.Next.Previous.GetType());
            Assert.AreNotEqual(null, po.Next.Next);
            Assert.AreEqual(typeof(ArrayObject), po.Next.Next.GetType());
        }
        #endregion
        #region quote
        [TestMethod]
        public void EmptyQuoteTest()
        {
            string text = "\"\"";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual("", ((SequenceObject)po).Text);
        }

        [TestMethod]
        public void SimpleQuoteTest()
        {
            string middle = "Ala ma kota";
            string text = "\"" + middle + "\"";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual(middle, ((SequenceObject)po).Text);
        }

        [TestMethod]
        public void QuoteTest()
        {
            string middle = "[Ala {ma} kota| ale nie ma psa]";
            string text = "\"" + middle + "\"";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual(middle, ((SequenceObject)po).Text);
        }

        [TestMethod]
        public void BackslashQuoteTest()
        {
            string middle = "Ala \\\" ma \\\\kotka";
            string text = "\"" + middle + "\"";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual("Ala \" ma \\kotka", ((SequenceObject)po).Text);
        }
        #endregion
        #region graph
        [TestMethod]
        public void NotGraphOrTreeTest()
        {
            string text = "treee, grapph";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual("treee,", ((SequenceObject)po).Text);
            Assert.AreNotEqual(null, po.Next);
            Assert.AreEqual(typeof(SequenceObject), po.Next.GetType());
            Assert.AreEqual("grapph", ((SequenceObject)po.Next).Text);
        }

        [TestMethod]
        public void SimplestGraphTest()
        {
            string text = "graph{1:cos;}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(GraphObject), po.GetType());
            Assert.AreEqual(1, ((GraphObject)po).VerticesNumber);
            Assert.IsFalse(((GraphObject)po).IsTree);
            Assert.AreEqual(0, ((GraphObject)po).EdgesNumber);
            Assert.AreEqual(typeof(SequenceObject), ((GraphObject)po).GetVertexObject(1).GetType());
            Assert.AreEqual("cos", ((SequenceObject)((GraphObject)po).GetVertexObject(1)).Text);
        }
        [TestMethod]
        public void DoubleTreeTest()
        {
            string text = "tree{1:a;2:b;=;1-2} tree{1:a;2:b;=;1-2}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(GraphObject), po.GetType());
            Assert.IsNotNull(po.Next);
        }

        [TestMethod]
        public void SimplestTreeTest()
        {
            string text = "tree{1:sth;}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(GraphObject), po.GetType());
            Assert.AreEqual(1, ((GraphObject)po).VerticesNumber);
            Assert.AreEqual(0, ((GraphObject)po).EdgesNumber);
            Assert.IsTrue(((GraphObject)po).IsTree);
            Assert.AreEqual(typeof(SequenceObject), ((GraphObject)po).GetVertexObject(1).GetType());
            Assert.AreEqual("sth", ((SequenceObject)((GraphObject)po).GetVertexObject(1)).Text);
        }

        [TestMethod]
        public void SimpleGraphTest()
        {
            string text = "graph{1:sth;3:{ten10}\n=\n1-1:hejka;1->3;}";
            var images = new Dictionary<string, Bitmap>();
            Bitmap bmp = new Bitmap(10, 10);
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            List<Edge> edges = new List<Edge>();
            edges.Add(new Edge(1, 1, false, "hejka"));
            edges.Add(new Edge(1, 3, true));
            Assert.AreEqual(typeof(GraphObject), po.GetType());
            Assert.AreEqual(2, ((GraphObject)po).VerticesNumber);
            Assert.AreEqual(2, ((GraphObject)po).EdgesNumber);
            foreach (var e in edges)
            {
                Assert.IsTrue(((GraphObject)po).ContainsEdge(e));
            }
        }

        [TestMethod]
        public void TreeTest()
        {
            string text = "tree {1: 7 2;\n 2: 3; \n3: 9 11;\n 4:2; \n5:  4 5 6; \n6: 8; \n7: 10;\n8: 13 15;\n=\n1-2;\n1-3;\n2-4;\n2-5; \n3-6;\n3-7;\n3-8;}";
            
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(GraphObject), po.GetType());
            Assert.AreEqual(8, ((GraphObject)po).VerticesNumber);
            Assert.AreEqual(7, ((GraphObject)po).EdgesNumber);
        }
        [TestMethod]
        public void GeneratingTreeTest()
        {
            string text = "tree { 1: Ala; 2: ma; 3: kota; 4: bo; 5: sierotka; 6: ma; 7: rysia; 8: fajnego;\n=\n1-2;1 - 3; 2 - 4; 2 - 5; 2 - 6; 3 - 7; 4 - 8;}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.IsInstanceOfType(po, typeof(GraphObject));
            TreeObject tree = new TreeObject(po as GraphObject);
            Assert.AreEqual(tree.Root.Node.Id, 1);
            Assert.IsTrue(tree.Root.Children.Count == 2);
            Assert.IsTrue(tree.Root.Children[0].Node.Id == 2 && tree.Root.Children[1].Node.Id == 3);
        }
        #endregion
        #region vector
        [TestMethod]
        public void SimpleVectorTest()
        {
            string text = "<.1:vec>start <1.> end";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(VectorObject), po.GetType());
            Assert.AreNotEqual(null, po.Next);
            Assert.AreEqual(typeof(VectorObject), po.Next.GetType());
            VectorObject vo1 = (VectorObject) po;
            VectorObject vo2 = (VectorObject)po.Next;
            Assert.AreEqual("vec", vo1.Label);
            Assert.IsTrue(vo1.IsStart);
            Assert.AreEqual(typeof(SequenceObject), vo1.Object.GetType());
            Assert.AreEqual(null, vo2.Label);
            Assert.IsFalse(vo2.IsStart);
            Assert.AreEqual(typeof(SequenceObject), vo2.Object.GetType());
        }
        [TestMethod]
        public void MultipleWordLabelTest()
        {
            string label = "many words label";
            string text = "<.1:"+label+">start <1.> end";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(VectorObject), po.GetType());
            Assert.AreNotEqual(null, po.Next);
            Assert.AreEqual(typeof(VectorObject), po.Next.GetType());
            VectorObject vo1 = (VectorObject)po;
            Assert.AreEqual(label, vo1.Label);
        }
        [TestMethod]
        public void SimpleReversedVectorTest()
        {
            string text = "<1.>start <.1:vec> end";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(VectorObject), po.GetType());
            Assert.AreNotEqual(null, po.Next);
            Assert.AreEqual(typeof(VectorObject), po.Next.GetType());
            VectorObject vo2 = (VectorObject)po;
            VectorObject vo1 = (VectorObject)po.Next;
            Assert.AreEqual("vec", vo1.Label);
            Assert.IsTrue(vo1.IsStart);
            Assert.AreEqual(typeof(SequenceObject), vo1.Object.GetType());
            Assert.AreEqual(null, vo2.Label);
            Assert.IsFalse(vo2.IsStart);
            Assert.AreEqual(typeof(SequenceObject), vo2.Object.GetType());
        }
        [TestMethod]
        public void VectorWithContinuationTest()
        {
            string text = "<.1:vec>start []<1.> end \"or not\"";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(VectorObject), po.GetType());
            Assert.AreNotEqual(null, po.Next);
            po = po.Next;
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
            Assert.AreEqual(typeof(VectorObject), po.Next.GetType());
            po = po.Next.Next;
            Assert.AreEqual(typeof(SequenceObject), po.GetType());
            Assert.AreEqual("or not", ((SequenceObject)po).Text);
        }

        [TestMethod]
        public void VectorInArrayTest()
        {
            string text = "[<.1:vec>start [] | \"or not\" <1.> end ]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            Assert.AreEqual(typeof(ArrayObject), po.GetType());
            Assert.AreEqual(2, ((ArrayObject)po).Count);
            ArrayObject ao = (ArrayObject) po;
            Assert.AreEqual(typeof(VectorObject), ao[0].GetType());
            Assert.AreEqual(typeof(ArrayObject), ao[0].Next.GetType());


            Assert.AreEqual(typeof(SequenceObject), ao[1].GetType());
            Assert.AreEqual(typeof(VectorObject), ao[1].Next.GetType());
            Assert.AreEqual("or not", ((SequenceObject)ao[1]).Text);
        }
        #endregion

        #region wrong tests

        protected void TestException(string text, Dictionary<string, Bitmap> images=null)
        {
            Parser.Parser p = new Parser.Parser(images);
            try
            {
                ParserObject po = p.ParseText(text);
                Assert.Fail();
            }
            catch (ErrorException ee)
            {
                
            }
        }

        [TestMethod]
        public void WrongArraysTest()
        {
            TestException("[arra|");
            //TestException("]");
            TestException("[[[[arra|]]]");
            TestException("[a[arra]|[][arra]");
        }

        [TestMethod]
        public void WrongImagesTest()
        {
            var images = new Dictionary<string, Bitmap>();
            Bitmap bmp = new Bitmap(10, 10);
            images.Add("ten10", bmp);
            TestException("{}", images);
            TestException("{ala}", images);
            TestException("[{ten10}|{ala}]", images);
        }

        [TestMethod]
        public void WrongVectorsTest()
        {
            //TestException(".2>");
            //TestException("2.>");
            TestException("<.2");
            TestException("<2.");
            TestException("<.2a>");
            TestException("<.a>");
        }

        [TestMethod]
        public void WrongQuotesTest()
        {
            TestException("\"");
            TestException("ala\"");
            TestException("\"\"ala\"");
        }
        [TestMethod]
        public void WrongGraphsTest()
        {
            //TestException("graph{1:cos}");
            TestException("graph{1cos;}");
            TestException("tree{1cos}");
            TestException("tree{1cos;}");
            TestException("graph{1:cos;2:a;1-2;}");
            //TestException("graph{1:cos;2:a;=1-2;}");
            //TestException("graph{1:cos;=;1-2;}");
            TestException("graph{1:cos;1-2;}");
            //TestException("tree{2:2;3:3;3:4 5 6;4:7;5:8;9:9 11; =; 2-3; 3-4;4-5;5-9}");
        }
        #endregion
    }
}
