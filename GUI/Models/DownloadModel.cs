﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUI.Models
{
    /// <summary>
    /// Download types
    /// </summary>
     public enum DownloadType
    {
        PDF, PNG, JPG
    }
}