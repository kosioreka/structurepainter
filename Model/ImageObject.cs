﻿using System.Drawing;

namespace Model
{
    /// <summary>
    /// Object containg images for visualization
    /// </summary>
    public class ImageObject : ParserObject
    {
        /// <summary>
        /// The image
        /// </summary>
        public Bitmap ImageBitmap { get; set; }
        
    }
}
