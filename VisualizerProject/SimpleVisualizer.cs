﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using Model;
using Shared;
using Brushes = System.Windows.Media.Brushes;
using Rectangle = System.Windows.Shapes.Rectangle;
using Size = System.Windows.Size;
using Point = System.Windows.Point;

namespace VisualizerProject
{
    /// <summary>
    /// Class for visualizing structures
    /// </summary>
    public class SimpleVisualizer : Visualizer
    {
        /// <summary>
        /// Vectors with one part visualized
        /// </summary>
        protected Dictionary<int, Canvas> Vectors;
        /// <summary>
        /// Labels of current vectors
        /// </summary>
        protected Dictionary<int, string> Labels;
        /// <summary>
        /// Starts of vectors - for visualization in array
        /// </summary>
        protected Dictionary<int, Canvas> Starts;
        /// <summary>
        /// Ends of vectors - for visualization in array
        /// </summary>
        protected Dictionary<int, Canvas> Ends;


        public SimpleVisualizer(Styles styles)
        {
            Styles = styles;
            Helper = new VisualizerHelper(styles);
            Vectors = new Dictionary<int, Canvas>();
            Starts = new Dictionary<int, Canvas>();
            Ends = new Dictionary<int, Canvas>();
            Labels = new Dictionary<int, string>();
        }
        #region arrays & hash tables
        /// <summary>
        /// Method for visualizing arrays
        /// </summary>
        /// <param name="obj">ArrayObject to visualize</param>
        /// <param name="isInArray">Whether the object is directly in an array</param>
        /// <returns>Visualization of the object in the argument</returns>
        public Canvas Visualize(ArrayObject obj, bool isInArray = false)
        {
            List<Canvas> cans = new List<Canvas>();
            int k = 0;
            foreach (var e in obj.Elements)
            {
                if (e != null)
                    cans.Add(Visualize(e, true));
                else
                    cans.Add(new Canvas());
                k++;
            }
            Canvas can = new Canvas();
            can.Background = Brushes.Transparent;
            double width, height;
            if (!obj.IsHashTable && !Styles.VerticalArray)
            {
                height = obj.Elements.Max(e => e?.MinSize.Height ?? 0) + 2 * (Styles.Margin + Styles.StrokeSize);
                width = obj.Elements.Sum(e => e?.MinSize.Width ?? 0) +
                               2 * obj.Elements.Count * Styles.Margin + (obj.Elements.Count + 1) * Styles.StrokeSize;
            }
            else
            {
                width = obj.Elements.Max(e => e?.MinSize.Width ?? 0) + 2 * (Styles.Margin + Styles.StrokeSize);
                height = obj.Elements.Sum(e => e?.MinSize.Height ?? 0) +
                               2 * obj.Elements.Count * Styles.Margin + (obj.Elements.Count + 1) * Styles.StrokeSize;
            }
            can.Height = height;
            can.Width = width;
            Rectangle rect = new Rectangle()
            {
                Width = width,
                Height = height,
                Stroke = Styles.LineColor,
                StrokeThickness = Styles.StrokeSize
            };

            Helper.AddToCanvas(can, rect, 0, 0);


            if (obj.IsHashTable)
                can = VisualizeHashTable(obj, cans, can);
            else
            {
                if (Styles.VerticalArray)
                    can = VisualizeVerticalArray(obj, cans, can);
                else
                    can = VisualizeHorizontalArray(obj, cans, can);

                //vectors
                List<int> toRemove = new List<int>();
                foreach (var n in Starts.Keys)
                {
                    if (Ends.ContainsKey(n))
                    {
                        try
                        {
                            Point start = Helper.GetVectorLocation(Starts[n], true);
                            Point end = Helper.GetVectorLocation(Ends[n], true);
                            if (Styles.VerticalArray)
                                can = DrawVerticalVector(can, start, end, Labels[n]);
                            else
                                can = DrawHorizontalVector(can, start, end, Labels[n]);
                        }
                        catch (KeyNotFoundException e)
                        {
                            Helper.ThrowDictionaryException(n);
                        }
                        toRemove.Add(n);
                    }
                }
                foreach (var n in toRemove)
                {
                    Starts.Remove(n);
                    Ends.Remove(n);
                    Labels.Remove(n);
                }
            }


            can = VisualizeNext(obj, can);
            obj.MinSize = new Size(can.Width, can.Height);
            return can;
        }
        /// <summary>
        /// Method to viualize hash table
        /// </summary>
        /// <param name="obj">ArrayObject to visualize</param>
        /// <param name="cans">Visualizations of arrays cells</param>
        /// <param name="can">Current visualization</param>
        /// <returns>Visualization of hash table</returns>
        public Canvas VisualizeHashTable(ArrayObject obj, List<Canvas> cans, Canvas can)
        {
            can = VisualizeVerticalArray(obj, cans, can);

            double minHashLength = Styles.ArrowHeight * 2 + Styles.StrokeSize;
            Dictionary<int, Canvas> hashes = new Dictionary<int, Canvas>();
            foreach (var key in obj.HashObjects.Keys)
            {
                hashes.Add(key, Visualize(obj.HashObjects[key].Object));
            }
            int multiStroke = 3;
            double sHeight = obj.HashObjects.Sum(e =>
                             {
                                 if (e.Value.Object != null && e.Value.IsStart)
                                     return e.Value.Object.MinSize.Height;
                                 return 0;
                             }) + (obj.HashObjects.Count - 1) * Styles.Margin;
            double eHeight = obj.HashObjects.Sum(e =>
                             {
                                 if (e.Value.Object != null && !e.Value.IsStart)
                                     return e.Value.Object.MinSize.Height;
                                 return 0;
                             }) + (obj.HashObjects.Count - 1) * Styles.Margin;

            double sWidth = obj.HashObjects.Max(e =>
                            {
                                if (e.Value.Object != null && e.Value.IsStart)
                                    return e.Value.Object.MinSize.Width;
                                return 0;
                            }) + minHashLength + obj.HashObjects.Count(e => e.Value.IsStart) * Styles.StrokeSize * multiStroke;

            double eWidth = obj.HashObjects.Max(e =>
                            {
                                if (e.Value.Object != null && !e.Value.IsStart)
                                    return e.Value.Object.MinSize.Width;
                                return 0;
                            }) + minHashLength + obj.HashObjects.Count(e => !e.Value.IsStart) * Styles.StrokeSize * multiStroke;
            eWidth = Math.Abs(eWidth - minHashLength) < 0.000000001 ? 0 : eWidth;
            sWidth = Math.Abs(sWidth - minHashLength) < 0.000000001 ? 0 : sWidth;
            Canvas newCan = new Canvas
            {
                Height = Math.Max(can.Height, Math.Max(sHeight, eHeight)),
                Width = can.Width + sWidth + eWidth,
                Background = Brushes.Transparent
            };

            double currentSHeight = (newCan.Height - sHeight) / 2;
            double currentEHeight = (newCan.Height - eHeight) / 2;
            double[] heights = new double[obj.Count];
            double height = 0;
            if (eHeight > can.Height || sHeight > can.Height)
            {
                height = (Math.Max(eHeight, sHeight) - can.Height + cans[0].Height)/2 + Styles.StrokeSize
                         + Styles.Margin;
            }
            else
            {
                height = Styles.StrokeSize + cans[0].Height/2+ Styles.Margin;
            }
            if (double.IsNaN(height))
            {
                height = Styles.Margin + Styles.StrokeSize;
            }
            heights[0] =  height;
            int starts = 0, ends = 0;
            for (int i = 1; i < obj.Count; i++)
            {
                heights[i] = heights[i - 1] + cans[i - 1].Height / 2 + cans[i].Height / 2 + Styles.Margin * 2 + Styles.StrokeSize;
            }
            foreach (var key in hashes.Keys)
            {
                double left;
                Point start, end;
                if (obj.HashObjects[key].IsStart)
                {
                    left = sWidth - minHashLength - hashes[key].Width;
                    Helper.AddToCanvas(newCan, hashes[key], left, currentSHeight);
                    start = new Point(left + hashes[key].Width, currentSHeight + hashes[key].Height / 2);
                    end = new Point(sWidth, heights[key]);
                    currentSHeight += Styles.Margin + hashes[key].Height;
                    Helper.DrawHash(newCan, start, end, Styles.ArrowHeight + Styles.StrokeSize * starts * multiStroke);
                    starts++;
                }
                else
                {
                    left = newCan.Width - eWidth + minHashLength;
                    Helper.AddToCanvas(newCan, hashes[key], left, currentEHeight);
                    end = new Point(left, currentEHeight + hashes[key].Height / 2);
                    start = new Point(sWidth + can.Width, heights[key]);
                    currentEHeight += Styles.Margin + hashes[key].Height;
                    Helper.DrawHash(newCan, start, end, Styles.ArrowHeight + Styles.StrokeSize * ends * multiStroke);
                    ends++;
                }
            }
            Helper.AddToCanvas(newCan, can, sWidth, (newCan.Height - can.Height) / 2);
            can = newCan;
            return can;
        }
        /// <summary>
        /// Method to visualize array in vertical position
        /// </summary>
        /// <param name="obj">ArrayObject to visualize</param>
        /// <param name="cans">Visualizations of arrays cells</param>
        /// <param name="can">Current visualization</param>
        /// <returns>Visualization of array in vertical position</returns>
        public Canvas VisualizeVerticalArray(ArrayObject obj, List<Canvas> cans, Canvas can)
        {
            double width = can.Width;
            double currentHeight = 0;
            for (int i = 0; i < obj.Elements.Count; i++)
            {
                double top = Styles.StrokeSize * (i + 1) + Styles.Margin * (2 * i + 1) + currentHeight;
                double w;
                if (obj.Elements[i] == null || double.IsNaN(obj.Elements[i].MinSize.Width))
                    w = 0;
                else
                    w = obj.Elements[i].MinSize.Width;
                double left = Math.Max(Styles.Margin,
                                 (width - 2 * Styles.StrokeSize - w) / 2)
                             + Styles.StrokeSize;
                Helper.AddToCanvas(can, cans[i], left, top);

                top = top + Styles.Margin;
                if (obj.Elements[i] != null)
                {
                    top += obj.Elements[i].MinSize.Height;
                    currentHeight += double.IsNaN(obj.Elements[i].MinSize.Height) ? 0 : obj.Elements[i].MinSize.Height;
                }
                Line line = Helper.CreateLine(width, true);

                Helper.AddToCanvas(can, line, 0, top + Styles.StrokeSize / 2);
            }
            return can;
        }

        /// <summary>
        /// Method to visualize array in horizontal position
        /// </summary>
        /// <param name="obj">ArrayObject to visualize</param>
        /// <param name="cans">Visualizations of arrays cells</param>
        /// <param name="can">Current visualization</param>
        /// <returns>Visualization of array in horizontal position</returns>
        public Canvas VisualizeHorizontalArray(ArrayObject obj, List<Canvas> cans, Canvas can)
        {

            double height = can.Height;

            double currentWidth = 0;
            for (int i = 0; i < obj.Elements.Count; i++)
            {
                double left = Styles.StrokeSize * (i + 1) + Styles.Margin * (2 * i + 1) + currentWidth;
                double h;
                if (obj.Elements[i] == null || double.IsNaN(obj.Elements[i].MinSize.Height))
                    h = 0;
                else
                    h = obj.Elements[i].MinSize.Height;
                double top = Math.Max(Styles.Margin,
                                 (height - 2 * Styles.StrokeSize - h) / 2)
                             + Styles.StrokeSize;
                Helper.AddToCanvas(can, cans[i], left, top);

                left = left + Styles.Margin;
                if (obj.Elements[i] != null)
                {
                    left += obj.Elements[i].MinSize.Width;
                    currentWidth += double.IsNaN(obj.Elements[i].MinSize.Width) ? 0 : obj.Elements[i].MinSize.Width;
                }
                Line line = Helper.CreateLine(height);

                Helper.AddToCanvas(can, line, left + Styles.StrokeSize / 2, 0);
            }

            return can;
        }
        #endregion
        #region other objects

        /// <summary>
        /// Method for visualizing images
        /// </summary>
        /// <param name="obj">ImageObject to visualize</param>
        /// <param name="isInArray">Whether the object is directly in an array</param>
        /// <returns>Visualization of the object in the argument</returns>
        public Canvas Visualize(ImageObject obj, bool isInArray = false)
        {
            Canvas can = new Canvas();
            if (!Styles.OriginalImageSize && (obj.ImageBitmap.Width > Styles.MaxSize.Width || obj.ImageBitmap.Height > Styles.MaxSize.Height))
            {
                double scale = Math.Min(Styles.MaxSize.Width / obj.ImageBitmap.Width,
                    Styles.MaxSize.Height / obj.ImageBitmap.Height);
                obj.ImageBitmap = new Bitmap(obj.ImageBitmap, (int)(obj.ImageBitmap.Width * scale), (int)(obj.ImageBitmap.Height * scale));
            }
            can.Width = obj.ImageBitmap.Width;
            can.Height = obj.ImageBitmap.Height;
            Rectangle rect = new Rectangle()
            {
                Width = obj.ImageBitmap.Width,
                Height = obj.ImageBitmap.Height,
                Fill = new ImageBrush(CanvasConverters.CreateBitmapSource(obj.ImageBitmap))
            };


            Helper.AddToCanvas(can, rect, 0, 0);
            can = VisualizeNext(obj, can);
            obj.MinSize = new Size(can.Width, can.Height);

            return can;
        }

        /// <summary>
        /// Method for visualizing text
        /// </summary>
        /// <param name="obj">SequenceObject to visualize</param>
        /// <param name="isInArray">Whether the object is directly in an array</param>
        /// <returns>Visualization of the object in the argument</returns>
        public Canvas Visualize(SequenceObject obj, bool isInArray = false)
        {
            var textBlock = new TextBlock
            {
                Text = obj.Text,
                TextWrapping = TextWrapping.Wrap,
                FontFamily = Styles.Font,
                FontSize = Styles.FontSize,
                Foreground = new SolidColorBrush(Styles.FontColor),
                Background = Brushes.Transparent
            };
            textBlock.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            textBlock.Arrange(new Rect(textBlock.DesiredSize));
            Canvas can = new Canvas()
            {
                Width = textBlock.ActualWidth,
                Height = textBlock.ActualHeight,
                Background = Brushes.Transparent
            };
            Helper.AddToCanvas(can, textBlock, 0, 0);

            can = VisualizeNext(obj, can);
            obj.MinSize = new Size(can.Width, can.Height);
            return can;
        }

        /// <summary>
        /// Method for visualizing vectors
        /// </summary>
        /// <param name="obj">VectorObject to visualize</param>
        /// <param name="isInArray">Whether the object is directly in an array</param>
        /// <returns>Visualization of the object in the argument</returns>
        public Canvas Visualize(VectorObject obj, bool isInArray = false)
        {
            Canvas can = Visualize(obj.Object);

            Canvas newCan = VisualizeNext(obj, can);
            try
            {


                if (Vectors.ContainsKey(obj.Number))
                {
                    Point start, end;
                    double top, left;

                    if (!Styles.VerticalFlow)
                    {
                        top = Canvas.GetTop(can);
                        left = can.Width/2;
                    }
                    else
                    {
                        left = Canvas.GetLeft(can);
                        top = can.Height/2;
                    }
                    if (obj.IsStart)
                    {
                        start = new Point(left, top);
                        end = Helper.GetVectorLocation(Vectors[obj.Number]);
                        Labels.Add(obj.Number, obj.Label);
                    }
                    else
                    {
                        end = new Point(left, top);
                        start = Helper.GetVectorLocation(Vectors[obj.Number]);
                    }
                    if (!isInArray && !double.IsNaN(top) && !double.IsNaN(left))
                    {

                        if (Styles.VerticalFlow)
                            can = DrawVerticalVector(newCan, start, end, Labels[obj.Number]);
                        else
                            can = DrawHorizontalVector(newCan, start, end, Labels[obj.Number]);
                        Labels.Remove(obj.Number);
                        Vectors.Remove(obj.Number);
                    }
                    else
                    {
                        if (obj.IsStart)
                        {
                            Starts.Add(obj.Number, can);
                            Ends.Add(obj.Number, Vectors[obj.Number]);
                        }
                        else
                        {
                            Ends.Add(obj.Number, can);
                            Starts.Add(obj.Number, Vectors[obj.Number]);
                        }
                        can = newCan;
                    }
                }
                else
                {
                    if (obj.IsStart)
                        Labels.Add(obj.Number, obj.Label);
                    Vectors.Add(obj.Number, can);
                    can = newCan;
                }
            }
            catch (Exception e) when (e is KeyNotFoundException || e is ArgumentException)
            {
                Helper.ThrowDictionaryException(obj.Number);
            }
            obj.MinSize = new Size(can.Width, can.Height);
            return can;

        }
        #endregion
        #region helpers
        /// <summary>
        /// Draws arrow for vector
        /// </summary>
        /// <param name="core">Actual canvas to draw on</param>
        /// <param name="start">Arrow start point</param>
        /// <param name="end">Arrow end point</param>
        /// <param name="label">Text of the vector label</param>
        /// <returns>Canvas element with drawn vector</returns>
        protected Canvas DrawHorizontalVector(Canvas core, Point start, Point end, string label)
        {
            Canvas labCanvas;
            double top = 0, left = 0, right = 0;
            if (label != null)
            {
                SequenceObject so = new SequenceObject(label);
                labCanvas = Visualize(so);
                top = labCanvas.Height + Styles.LabelMargin;
            }
            else
            {
                labCanvas = new Canvas
                {
                    Height = 0,
                    Width = 0
                };
            }
            //if label should start earlier than the rest
            left = Math.Abs(start.X + end.X) / 2 - labCanvas.Width / 2;
            if (left < 0)
            {
                left = -left;
            }
            else
            {
                left = 0;
            }
            //if label ends later than the rest
            right = Math.Abs(start.X + end.X) / 2 + labCanvas.Width / 2;
            if (right < core.Width)
            {
                right = core.Width;
            }

            Canvas can = new Canvas
            {
                Height = core.Height + Styles.ArrowHeight + Styles.StrokeSize + top,
                Width = right + left
            };
            Helper.AddToCanvas(can, labCanvas, Math.Max(0, Math.Abs(start.X + end.X) / 2 - labCanvas.Width / 2), 0);
            Helper.AddToCanvas(can, core, left, Styles.ArrowHeight + Styles.StrokeSize + top);

            double x = Math.Cos(Styles.ArrowAngle) * Styles.ArrowLength;
            double y = Math.Sin(Styles.ArrowAngle) * Styles.ArrowLength;
            Line line;

            line = Helper.CreateLine(Styles.ArrowHeight + start.Y);
            Helper.AddToCanvas(can, line, start.X + left, top, -1);
            line = Helper.CreateLine(Styles.ArrowHeight + end.Y);
            Helper.AddToCanvas(can, line, end.X + left, top, -1);
            //horizontal line
            line = Helper.CreateLine(Math.Abs(start.X - end.X), true);
            Helper.AddToCanvas(can, line, Math.Min(start.X, end.X) + left, top > 0 ? top : Styles.StrokeSize, -1);


            double leftArrow = end.X - x + left;
            double rightArrow = end.X + left;
            //arrow
            line = Helper.CreateLine(0, 0, x, y);
            Helper.AddToCanvas(can, line, leftArrow, end.Y + Styles.ArrowHeight + Styles.StrokeSize - y + top, -1);

            line = Helper.CreateLine(0, y, x, 0);
            Helper.AddToCanvas(can, line, rightArrow, end.Y + Styles.ArrowHeight + Styles.StrokeSize - y + top, -1);

            if (leftArrow < 0)
            {
                Canvas temp = new Canvas()
                {
                    Height = can.Height,
                    Width = can.Width - leftArrow
                };
                Helper.AddToCanvas(temp, can, -leftArrow, 0);
                can = temp;

            }
            if (rightArrow + x > can.Width)
            {
                Canvas temp = new Canvas()
                {
                    Height = can.Height,
                    Width = rightArrow + x
                };
                Helper.AddToCanvas(temp, can, 0, 0);
                can = temp;
            }

            return can;
        }
        /// <summary>
        /// Draws arrow for vector
        /// </summary>
        /// <param name="core">Actual canvas to draw on</param>
        /// <param name="start">Arrow start point</param>
        /// <param name="end">Arrow end point</param>
        /// <param name="label">Text of the vector label</param>
        /// <returns>Canvas element with drawn vector</returns>
        protected Canvas DrawVerticalVector(Canvas core, Point start, Point end, string label)
        {
            Canvas labCanvas;
            double left = 0;
            if (label != null)
            {
                SequenceObject so = new SequenceObject(label);
                labCanvas = Visualize(so);
                left = labCanvas.Width + Styles.LabelMargin;
            }
            else
            {
                labCanvas = new Canvas
                {
                    Height = 0,
                    Width = 0
                };
            }

            Canvas can = new Canvas
            {
                Height = Math.Max(core.Height, labCanvas.Height),
                Width = left + core.Width + Styles.ArrowHeight + Styles.StrokeSize
            };
            Helper.AddToCanvas(can, labCanvas, 0, Math.Max(0, Math.Abs(start.Y + end.Y) / 2 - labCanvas.Height / 2));
            Helper.AddToCanvas(can, core, left + Styles.ArrowHeight + Styles.StrokeSize, Math.Max(0, -(core.Height - labCanvas.Height) / 2));

            double x = Math.Cos(Styles.ArrowAngle) * Styles.ArrowLength;
            double y = Math.Sin(Styles.ArrowAngle) * Styles.ArrowLength;
            Line line;

            line = Helper.CreateLine(Styles.ArrowHeight + start.X, true);
            Helper.AddToCanvas(can, line, left, start.Y, -1);
            line = Helper.CreateLine(Styles.ArrowHeight + end.X, true);
            Helper.AddToCanvas(can, line, left, end.Y, -1);
            //vertical line
            line = Helper.CreateLine(Math.Abs(start.Y - end.Y));
            Helper.AddToCanvas(can, line, left > 0 ? left : Styles.StrokeSize, Math.Min(start.Y, end.Y), -1);

            double topArrow = end.Y - y;
            //arrow
            line = Helper.CreateLine(0, 0, x, y);
            Helper.AddToCanvas(can, line, end.X - x + left + Styles.ArrowHeight, topArrow, -1);

            line = Helper.CreateLine(0, x, y, 0);
            Helper.AddToCanvas(can, line, end.X - x + left + Styles.ArrowHeight, end.Y, -1);

            if (topArrow < 0)
            {
                Canvas temp = new Canvas()
                {
                    Height = can.Height - topArrow,
                    Width = can.Width
                };
                Helper.AddToCanvas(temp, can, 0, -topArrow);
                can = temp;

            }
            if (end.Y + x > can.Width)
            {
                Canvas temp = new Canvas()
                {
                    Height = end.Y + x,
                    Width = can.Width
                };
                Helper.AddToCanvas(temp, can, 0, 0);
                can = temp;
            }

            return can;
        }

        /// <summary>
        /// Visualizes next objects on the same level
        /// </summary>
        /// <param name="po">Curren ParserObject</param>
        /// <param name="can">Visualization of curren object</param>
        /// <returns>Visualization of current object with added visualization of next objects</returns>
        protected Canvas VisualizeNext(ParserObject po, Canvas can)
        {

            if (po.Next == null)
                return can;

            Canvas next = Visualize(po.Next);
            Canvas main;

            if (Styles.VerticalFlow)
            {
                main = new Canvas
                {
                    Width = Math.Max(next.Width, can.Width),
                    Height = can.Height + Styles.Margin + next.Height
                };
                double left = (main.Width - can.Width) / 2;
                Helper.AddToCanvas(main, can, left, 0);
                left = (main.Width - next.Width) / 2;
                Helper.AddToCanvas(main, next, left, can.Height + Styles.Margin);
            }
            else
            {
                main = new Canvas
                {
                    Height = Math.Max(next.Height, can.Height),
                    Width = can.Width + Styles.Margin + next.Width
                };
                double top = (main.Height - can.Height) / 2;
                Helper.AddToCanvas(main, can, 0, top);
                top = (main.Height - next.Height) / 2;
                Helper.AddToCanvas(main, next, can.Width + Styles.Margin, top);
            }


            return main;
        }
        #endregion
        #region Graphs
        /// <summary>
        /// Method for visualizing graphs
        /// </summary>
        /// <param name="obj">GraphObject to visualize</param>
        /// <param name="isInArray">Whether the object is directly in an array</param>
        /// <returns>Visualization of the object in the argument</returns>
        public Canvas Visualize(GraphObject obj, bool isInArray = false)
        {
            if (obj.IsTree)
            {
                var tree = new TreeObject(obj);
                if (tree.Generated)
                {
                    Canvas can = Visualize(tree);
                    can = VisualizeNext(obj, can);
                    obj.MinSize = new Size(can.Width, can.Height);
                    return can;
                }
                //else
                //{
                //    if (obj.IsTree)
                //    {
                //        throw new Exception("Podana struktura nie jest drzewem. Sprawdź zdefiniowane krawędzie. Obowiązuje kolejność zstępująca.");
                //    }
                //    throw new Exception("Przepraszamy. Generowanie grafów nie jest dostępne."); 
                //}
            }
            Dictionary<int, Canvas> canvases = new Dictionary<int, Canvas>();
            foreach (var v in obj.Vertices)
            {
                var can = Visualize(v.Object);
                v.Object.MinSize = new Size(9 / 5 * Styles.NodeMarginWidth + 3 * Styles.StrokeSize + 1.4 * v.Object.MinSize.Width,
                            3 / 2 * Styles.NodeMarginHeight + 3 * Styles.StrokeSize + 1.4 * v.Object.MinSize.Height);
                canvases.Add(v.Id, can);
            }
            GraphHelper gHelper = new GraphHelper(Styles);
            gHelper.GenerateGraph(obj);
            foreach (var v in obj.Vertices)
            {
                int k = v.Id;
                canvases[k] = Helper.SetObjectInNode(canvases[k]);
            }
            Canvas canvas = new Canvas()
            {
                Width = obj.MinSize.Width + Styles.Margin,
                Height = obj.MinSize.Height + Styles.Margin,
                Background = Brushes.Transparent
            };
            foreach (var v in obj.Vertices)
            {
                var can = canvases[v.Id];
                Helper.AddToCanvas(canvas, can, v.Point.X - can.Width / 2 + Styles.Margin / 2,
                                    v.Point.Y - can.Height / 2 + Styles.Margin / 2);
            }
            for (int i = 0; i < obj.EdgesNumber; ++i)
            {
                var edge = obj.GetEdge(i);
                Path p = Helper.DrawBezierCurves(edge.Points);
                Helper.AddToCanvas(canvas, p, 0, 0);

                if (edge.Label != null && edge.Label != "")
                {
                    var labelObj = new SequenceObject(edge.Label);
                    var can = Visualize(labelObj);
                    Helper.AddToCanvas(canvas, can, edge.LabelPoint.X - can.Width / 2, edge.LabelPoint.Y);
                }
                if (edge.IsDirected)
                {
                    var count = edge.Points.Count;
                    Helper.AddToCanvas(canvas, Helper.CreateGraphArrow(edge.Points[count - 1], edge.Points[count - 2]), 0, 0);
                }
            }

            canvas = VisualizeNext(obj, canvas);
            obj.MinSize = new Size(canvas.Width, canvas.Height);
            return canvas;
        }
        /// <summary>
        /// Method for visualizing trees
        /// </summary>
        /// <param name="obj">TreeObject to visualize</param>
        /// <param name="isInArray">Whether the object is directly in an array</param>
        /// <returns>Visualization of the object in the argument</returns>
        public Canvas Visualize(TreeObject obj, bool isInArray = false)
        {
            return Visualize(obj.Root);
        }
        /// <summary>
        /// Method for visualizing tree nodes
        /// </summary>
        /// <param name="obj">TreeNodeObject to visualize</param>
        /// <returns>Visualization of the object in the argument</returns>
        public Canvas Visualize(TreeNodeObject obj)
        {
            var can = Visualize(obj.Node.Object);
            can = Helper.SetObjectInNode(can, obj);
            if (obj.Children != null && obj.Children.Any())
            {
                List<Canvas> cans = new List<Canvas>();

                foreach (var n in obj.Children)
                {
                    cans.Add(Visualize(n));
                }
                double height = obj.Children.Max(e => e?.MinSize.Height ?? 0);
                double maxWidth = obj.Children.Max(e => e?.MinSize.Width ?? 0);
                double width = obj.Children.Count * (Styles.StrokeSize + maxWidth);

                Canvas childCan = new Canvas()
                {
                    Height = height,
                    Width = width,
                    Background = Brushes.Transparent
                };
                double left = Styles.StrokeSize - 1;
                int i = 0;
                foreach (var c in cans)
                {
                    var leftC = left + (maxWidth - c.Width) / 2; //centering
                    var childObj = obj.Children[i++];
                    Helper.CombineNodeCanvases(childCan, c, ref childObj, leftC, 0);
                    left += maxWidth + Styles.StrokeSize; //translation to right
                }
                var canWidth = Math.Max(width, can.Width);
                Canvas canvas = new Canvas()
                {
                    Background = Brushes.Transparent,
                    Width = canWidth,
                    Height = height + can.Height + 2 * Styles.Margin + 2 * Styles.StrokeSize
                };
                //Translate only parent
                Helper.CombineNodeCanvases(canvas, can, ref obj, (canWidth - can.Width) / 2, 0, 1);
                //Translate only children           
                Helper.CombineNodeCanvases(canvas, childCan, ref obj, (canWidth - width) / 2, can.Height + 2 * Styles.Margin, 2);

                Helper.AddTreeEdges(canvas, obj);

                obj.MinSize = new Size(canvas.Width, canvas.Height);
                return canvas;
            }
            obj.MinSize = new Size(can.Width, can.Height);
            return can;
        }

        #endregion

        /// <summary>
        /// Method to visualize structure
        /// </summary>
        /// <param name="po">The root of the structure to visualize</param>
        /// <returns>Visualization of the structure</returns>
        public override Canvas Visualize(ParserObject po)
        {
            return Visualize(po, false);

        }
        /// <summary>
        /// Method to visualize structure
        /// </summary>
        /// <param name="po">The root of the structure to visualize</param>
        /// <param name="isInArray">Whether the structure is in array</param>
        /// <returns>Visualization of the structure</returns>
        public Canvas Visualize(ParserObject po, bool isInArray)
        {
            if (po is ArrayObject)
            {
                return Visualize((ArrayObject)po, isInArray);
            }
            if (po is ImageObject)
            {
                return Visualize((ImageObject)po, isInArray);
            }
            if (po is SequenceObject)
            {
                return Visualize((SequenceObject)po, isInArray);
            }
            if (po is GraphObject)
            {
                return Visualize((GraphObject)po, isInArray);
            }
            if (po is VectorObject)
            {
                return Visualize((VectorObject)po, isInArray);
            }

            Canvas can = new Canvas
            {
                Height = 0,
                Width = 0
            };
            return can;
        }
    }
}
