var hierarchy =
[
    [ "ApplicationException", null, [
      [ "Parser.ErrorException", "class_parser_1_1_error_exception.html", null ]
    ] ],
    [ "GUI.BundleConfig", "class_g_u_i_1_1_bundle_config.html", null ],
    [ "Shared.CanvasConverters", "class_shared_1_1_canvas_converters.html", null ],
    [ "Controller", null, [
      [ "GUI.Controllers.StructureGeneratorController", "class_g_u_i_1_1_controllers_1_1_structure_generator_controller.html", null ],
      [ "GUI.Controllers.StructurePainterController", "class_g_u_i_1_1_controllers_1_1_structure_painter_controller.html", null ]
    ] ],
    [ "GUI.Helpers.DatabaseReader", "class_g_u_i_1_1_helpers_1_1_database_reader.html", null ],
    [ "DynamicObject", null, [
      [ "ModuleTests.JsonResultDynamicWrapper", "class_module_tests_1_1_json_result_dynamic_wrapper.html", null ]
    ] ],
    [ "EXSTYPE", "union_e_x_s_t_y_p_e.html", null ],
    [ "GUI.FilterConfig", "class_g_u_i_1_1_filter_config.html", null ],
    [ "GUI.Helpers.GeneratorHelper", "class_g_u_i_1_1_helpers_1_1_generator_helper.html", null ],
    [ "VisualizerProject.GraphHelper", "class_visualizer_project_1_1_graph_helper.html", null ],
    [ "VisualizerProject.Graphviz", "class_visualizer_project_1_1_graphviz.html", null ],
    [ "ModuleTests.GUITests", "class_module_tests_1_1_g_u_i_tests.html", null ],
    [ "Model.HashWrapper", "class_model_1_1_hash_wrapper.html", null ],
    [ "HttpApplication", null, [
      [ "GUI.MvcApplication", "class_g_u_i_1_1_mvc_application.html", null ]
    ] ],
    [ "IdentityDbContext", null, [
      [ "GUI.Models.ApplicationDbContext", "class_g_u_i_1_1_models_1_1_application_db_context.html", null ]
    ] ],
    [ "IdentityUser", null, [
      [ "GUI.Models.ApplicationUser", "class_g_u_i_1_1_models_1_1_application_user.html", null ]
    ] ],
    [ "IEquatable", null, [
      [ "Model.Edge", "class_model_1_1_edge.html", null ]
    ] ],
    [ "IIdentityMessageService", null, [
      [ "GUI.EmailService", "class_g_u_i_1_1_email_service.html", null ],
      [ "GUI.SmsService", "class_g_u_i_1_1_sms_service.html", null ]
    ] ],
    [ "Shared.ImageConverters", "class_shared_1_1_image_converters.html", null ],
    [ "Parser.Parser", "class_parser_1_1_parser.html", null ],
    [ "Model.ParserObject", "class_model_1_1_parser_object.html", [
      [ "Model.ArrayObject", "class_model_1_1_array_object.html", null ],
      [ "Model.GraphObject", "class_model_1_1_graph_object.html", null ],
      [ "Model.ImageObject", "class_model_1_1_image_object.html", null ],
      [ "Model.SequenceObject", "class_model_1_1_sequence_object.html", null ],
      [ "Model.TreeNodeObject", "class_model_1_1_tree_node_object.html", null ],
      [ "Model.TreeObject", "class_model_1_1_tree_object.html", null ],
      [ "Model.VectorObject", "class_model_1_1_vector_object.html", null ]
    ] ],
    [ "GUI.Models.PictureUploadModel", "class_g_u_i_1_1_models_1_1_picture_upload_model.html", null ],
    [ "GUI.RouteConfig", "class_g_u_i_1_1_route_config.html", null ],
    [ "Parser.Scanner", "class_parser_1_1_scanner.html", null ],
    [ "ModuleTests.ScanTests", "class_module_tests_1_1_scan_tests.html", null ],
    [ "SignInManager", null, [
      [ "GUI.ApplicationSignInManager", "class_g_u_i_1_1_application_sign_in_manager.html", null ]
    ] ],
    [ "GUI.Startup", "class_g_u_i_1_1_startup.html", null ],
    [ "GUI.Models.StructureGeneratorModel", "class_g_u_i_1_1_models_1_1_structure_generator_model.html", null ],
    [ "GUI.Models.StyleModel", "class_g_u_i_1_1_models_1_1_style_model.html", null ],
    [ "Model.Styles", "class_model_1_1_styles.html", null ],
    [ "UserManager", null, [
      [ "GUI.ApplicationUserManager", "class_g_u_i_1_1_application_user_manager.html", null ]
    ] ],
    [ "Model.Vertex", "class_model_1_1_vertex.html", null ],
    [ "ModuleTests.VisualizationTests", "class_module_tests_1_1_visualization_tests.html", null ],
    [ "VisualizerProject.Visualizer", "class_visualizer_project_1_1_visualizer.html", [
      [ "VisualizerProject.SimpleVisualizer", "class_visualizer_project_1_1_simple_visualizer.html", null ]
    ] ],
    [ "VisualizerProject.VisualizerHelper", "class_visualizer_project_1_1_visualizer_helper.html", null ]
];