﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Controls;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuickGraph;
using QuickGraph.Graphviz;
using VisualizerProject;
using Shared;
using Model;

namespace ModuleTests
{
    [TestClass]
    public class VisualizationTests
    {
        #region basic
        [TestMethod]
        public void ImageTest()
        {
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.Crimson, ImageSize);
            }
            string text = "{ten10}";
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("ImageTest.png");
        }

        [TestMethod]
        public void TextTest()
        {
            Bitmap bmp;
            string text = "alamakota";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("TextTest.png");
        }

        [TestMethod]
        public void SimpleArrayTest()
        {
            Bitmap bmp;
            string text = "[||]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("SimpleArrayTest.png");
        }
        [TestMethod]
        public void SimpleDoubleArrayTest()
        {
            Bitmap bmp;
            string text = "[][]\n[||][||][||]";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("SimpleDoubleArrayTest.png");
        }

        [TestMethod]
        public void SimpleDoubleSequenceTest()
        {
            Bitmap bmp;
            string text = "Ala ma kota";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("SimpleDoubleSequenceTest.png");
        }
        [TestMethod]
        public void ArrayTest()
        {
            int width = 100, height = 100;
            Bitmap bmp;
            bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DeepSkyBlue, ImageSize);
            }
            //string text = "[ala|ma]";
            string text = "[[ala|ma]|kota Mruczka|[a]|[ale | {ten10}]]";
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("ArrayTest.png");
        }


        [TestMethod]
        public void SimpleQuoteTest()
        {
            string middle = "Ala ma kota";
            string text = "\"" + middle + "\"";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("SimpleQuoteTest.png");
        }

        [TestMethod]
        public void QuoteTest()
        {
            string middle = "< . 1 > \\ \\ \\ \" tree{}[Ala {ma} kota| ale nie ma psa]";
            string text = "\"" + middle + "\"";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("QuoteTest.png");
        }
        #endregion
        #region graph
        [TestMethod]
        public void TreeTest()
        {
            string text = "tree { 1: Ala; 2: ma; 3: kota; 4: bo; 5: sierotka; 6: ma; 7: rysia; 8: fajnego;\n=\n1-2:6;1 -> 3:4; 2 - 4:1; 2 - 5:uuu; 2 - 6:7; 3 - 7:bool; 6 - 8:a}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("TreeTest.png");
        }
        [TestMethod]
        public void TreeWidthTest()
        {
            string text = "tree { 1:[Ala | ma | kota] ; 2:{ten10}; 3:pieska; 4:1; 5:oooo! =; 1->2:ma; 1->3:nie ma; 3-4; 2->5;}";
            int width = 100, height = 100;
            Bitmap img = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(img))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.Blue, ImageSize);
            }

            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", img);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("TreeWidthTest.png");
        }
        [TestMethod]
        public void TreeSimpleTest()
        {
            string text = "tree{ 1:Ala ; 2:kota; 3:pieska; =; 1->2:ma; 1->3:nie ma;}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("TreeSimpleTest.png");
        }
        [TestMethod]
        public void TreeLabelTest()
        {
            string text = "tree { 1: Ala; 2: ma; 3: kota; 4: bo; 5: si; 6: ma; 7: rysia; 8: f;\n=\n1-2:kto;1 -> 3:fajnego; 2 - 4:aaaaaaaaaaaa; 2 - 5:uuu; 2 - 6:eeeeeaaaaajsjs278382773ehhhh; 3 - 7:bool; 6 - 8:hshwajnsssssssssssss}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("TreeLabelTest.png");
        }
        [TestMethod]
        public void TreeLabelCityTest()
        {
            string text = "tree{ 1:Warszawa ; 2:Gdańsk; 3:Kraków; 4:Poznań; =; 1-2:283 kmmmm; 1-3:252 kmm; }";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("TreeLabelCityTest.png");
        }
        [TestMethod]
        public void NoRootTreeTest()
        {
            string text = "tree { 1: Ala; 2: ma; 3: kota; 4: bo; 5: sierotka; 6: ma; 7: rysia; 8: fajnego;\n=\n1-2;1 - 3; 2 - 4; 2 - 5; 2 - 6; 6 - 1; 3 - 7; 6 - 8;}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("NoRootTreeTest.png");
        }
        [TestMethod]
        public void WrongTreeTest()
        {
            string text = "tree { 1: Ala; 2: ma; 3: kota; 4: bo; 5: sierotka; 6: ma; 7: rysia; 8: fajnego;\n=\n1-2;1 - 3; 2 - 4; 2 - 5; 2 - 6; 6 - 7; 3 - 7; 6 - 8;}";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("WrongTreeTest.png");
        }
        [TestMethod]
        public void GraphvizTest()
        {
            var edges = new SEdge<int>[] {
                new SEdge<int>(1, 2),
                new SEdge<int>(0, 1),
                new SEdge<int>(0, 3),
                new SEdge<int>(2, 3)
            };
            var graph = edges.ToAdjacencyGraph<int, SEdge<int>>();
            //var graphviz = new GraphvizAlgorithm<TVertex, TEdge>(g);

            //graphviz.Generate();
            //QuickGraph.Graphviz.Dot.
            var graphVizString = graph.ToGraphviz();
            var graphString = "digraph G {\r\n0;\r\n1 [shape=rectangle];\r\n2;\r\n3;\r\n0-> 1[];\r\n1-> 3[];\r\n2-> 0[];\r\n2-> 3[];\r\n}\r\n";
            try
            {
                Graphviz graphviz = new Graphviz();
                graphviz.RenderGraphvizString(graphString, "dot", "png");
                var gh = new GraphHelper(new Styles());
                //gh.GenerateGraph();
            }
            catch (Exception e)
            {

            }
        }
        [TestMethod]
        public void RenderSimpleGraphFromGraphviz()
        {
            var text = "graph { 1: Ala; 2: ma; \n=\n 1 -> 2; }";

            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("GraphSimpleTest.png");
        }
        [TestMethod]
        public void RenderCitiesGraphFromGraphviz()
        {
            var text = "graph{ 1:Warszawa; 2:Gdańsk; 3:Kraków; =; 1-2:283 km; 1-3:252 km; 2-3: 485 km;} ";

            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("GraphCityTest.png");
        }
        [TestMethod]
        public void RenderGraphFromGraphviz()
        {
            var text = "graph { 1: Alaaaaaa; 2: ma; 3: kota; 4: bo; 5: sierotka; 6: ma; 7: rysia; 8: fajnego;\n=\n 1 -> 2: uszatek; 1 -> 3; 2 -> 4: looooooooooleks; 2 -> 5; 2 - 6; 6 -> 1; 3 - 7; 6 -> 8; 8 - 4;}";

            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            Bitmap bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("GraphTest.png");
        }

        #endregion
        #region vector
        [TestMethod]
        public void SimpleVectorTest()
        {
            Bitmap bmp;
            string text = "< 2 . >koniec wektora < . 2 : dwa > start ";
            //string text = "start end";
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("SimpleVectorTest.png");
        }

        [TestMethod]
        public void DifferentHeightVectorTest()
        {
            string text = "<.1:2>{ten10}<1.>2";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("DifferentHeightVectorTest.png");
        }

        [TestMethod]
        public void DifferentHeightVectorMirrorTest()
        {
            string text = "<1.>2 <.1:vec> {ten10} ";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("DifferentHeightVectorMirrorTest.png");
        }
        [TestMethod]
        public void LongVectorLabelTest()
        {
            string text = "<.1:baaaaaardzo długi opis wektora>start <1.> {ten10}";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("LongVectorLabelTest.png");
        }
        [TestMethod]
        public void SimpleVectorWithMiddleTest()
        {
            string text = "<.1:vec>start {ten10} jakiś tekst <1.> [koniec]";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("SimpleVectorWithMiddleTest.png");
        }
        [TestMethod]
        public void DoubleVectorTest()
        {
            string text = "<.1:vec>start {ten10} <1.>jakiś <2.>tekst <.2> [koniec]";
            int width = 1000, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("DoubleVectorTest.png");
        }
        [TestMethod]
        public void DoubleVectorCrossTest()
        {
            string text = "< 2 . >a {ten10} < . 1 > b < . 2 : dwa> c < 1. > d";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("DoubleVectorCrossTest.png");
        }

        [TestMethod]
        public void VectorBiggerAfterTest()
        {
            string text = "<.1:vec>start <1.>koniec  [cos]";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("VectorBiggerAfterTest.png");
        }
        [TestMethod]
        public void VectorInArrayTest()
        {
            string text = "[<.1:vec>start jakiś |tekst <1.> koniec]";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("VectorInArrayTest.png");
        }
        [TestMethod]
        public void VectorInEmptyArrayTest()
        {
            string text = "[ < 2 . > | | < . 2 : dwa>]";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("VectorInEmptyArrayTest.png");
        }
        [TestMethod]
        public void VectorInArray2Test()
        {
            string text = "[ < 2 . >koniec | | < . 2 : dwa > start wektora]";
            //string text = "< 2 . >start < . 2 : dwa> < . 1 > koniec < 1 .> wektora";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("VectorInArray2Test.png");
        }

        [TestMethod]
        public void TwoVectorKindsTest()
        {
            string text = "[ < 2 . > a | | < . 2 : dwa > b ] < . 1 > c < 1 . > d ";
            //string text = "< 2 . >start < . 2 : dwa> < . 1 > koniec < 1 .> wektora";
            int width = 100, height = 100;
            Bitmap bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DarkOrchid, ImageSize);
            }
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("TwoVectorKindsTest.png");
        }
        [TestMethod]
        public void VectorToVectorTest()
        {
            string text = "< 2 . > start < . 2 : dwa > < . 1 > koniec < 1 . > wektora";

            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("VectorToVectorTest.png");
        }
        #endregion
        #region hash tables
        [TestMethod]
        public void EmptyHashTableTest()
        {

            string text = "[>[]]";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("EmptyHashTableTest.png");
        }
        [TestMethod]
        public void EmptyHashTableTest2()
        {

            string text = "[()>[]]";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("EmptyHashTableTest2.png");
        }
        [TestMethod]
        public void EmptyHashTableTest3()
        {

            string text = "[[]>]";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("EmptyHashTableTest3.png");
        }
        [TestMethod]
        public void EmptyHashTableTest4()
        {

            string text = "[([])>]";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("EmptyHashTableTest4.png");
        }
        [TestMethod]
        public void EmptyHashTableTest5()
        {

            string text = "[>]";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("EmptyHashTableTest5.png");
        }
        [TestMethod]
        public void EmptyHashTableTest6()
        {

            string text = "[()>]";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("EmptyHashTableTest6.png");
        }
        [TestMethod]
        public void SimpleHashTableTest()
        {

            string text = "[ala>ma kota| Mruczka]";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            //CanvasConverters.SaveCanvasAsPdf(can);
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("SimpleHashTableTest.png");
        }

        [TestMethod]
        public void SimpleHashMirrorTableTest()
        {

            string text = "[(ala)>ma kota| Mruczka]";
            Bitmap bmp;
            Parser.Parser p = new Parser.Parser(null);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("SimpleHashMirrorTableTest.png");
        }
        [TestMethod]
        public void HashTableTest()
        {
            int width = 100, height = 100;
            Bitmap bmp;
            bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DeepSkyBlue, ImageSize);
            }
            string text = "[([ala|ma])>{ten10}|kota>Mruczka|a>[a]|[ale > {ten10}] | " +
                "tree { 1: Ala; 2: ma; 3: kota; 4: bo; 5:tak;6:chce \n=\n1-2;1 - 3; 3 - 4;4 - 5;5 - 6;}>drzewko|cos]";
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("HashTableTest.png");
        }
        [TestMethod]
        public void HashTableTest2()
        {
            int width = 100, height = 100;
            Bitmap bmp;
            bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DeepSkyBlue, ImageSize);
            }
            string text = "[ 1 > 2 | 4 | 5 | 7 >  8 | 9 > 10 ] ";
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles { Size = StructureSize.Huge });
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("HashTableTest2.png");
        }

        [TestMethod]
        public void HashTableTest3()
        {
            int width = 300, height = 300;
            Bitmap bmp;
            bmp = new Bitmap(width, height);
            using (Graphics graph = Graphics.FromImage(bmp))
            {
                Rectangle ImageSize = new Rectangle(0, 0, width, height);
                graph.FillRectangle(Brushes.DeepSkyBlue, ImageSize);
            }
            string text = "[Ala> {ten10} | 2 | 3]  ";
            var images = new Dictionary<string, Bitmap>();
            images.Add("ten10", bmp);
            Parser.Parser p = new Parser.Parser(images);
            ParserObject po = p.ParseText(text);
            SimpleVisualizer sv = new SimpleVisualizer(new Styles());
            Canvas can = sv.Visualize(po);
            can.Background = System.Windows.Media.Brushes.White;
            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
            bmp.Save("HashTableTest3.png");
        }
        #endregion
    }
}
