﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Shared
{
    /// <summary>
    /// Class with helper methods for converting images to specified format
    /// </summary>
    public class ImageConverters
    {
        /// <summary>
        /// Converts input stream to base64 string
        /// </summary>
        /// <param name="stream">The stream to read its content</param>
        /// <returns>String in base64</returns>
        public static string FileToBase64String(Stream stream)
        {
            string data;
            using (Stream inputStream = stream)
            {
                MemoryStream memoryStream = inputStream as MemoryStream;
                if (memoryStream == null)
                {
                    memoryStream = new MemoryStream();
                    inputStream.CopyTo(memoryStream);
                }
                data = Convert.ToBase64String(memoryStream.ToArray());
            }
            return data;
        }
        /// <summary>
        /// Converts array of bytes into a bitmap
        /// </summary>
        /// <param name="bytes">Array of bytes to convert</param>
        /// <returns>The converted bitmap</returns>
        public static Bitmap Base64StringToBitmap(byte[] bytes)
        {
            Bitmap bmp;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                bmp = new Bitmap(ms);
            }

            return bmp;
        }

        /// <summary>
        /// Converts bitmap to base64 string
        /// </summary>
        /// <param name="bmp">Bitmap to convert</param>
        /// <returns>The bitmap as base 64 string</returns>
        public static string BitmapToBase64String(Bitmap bmp)
        {
            return Convert.ToBase64String(BitmapToByteArray(bmp, ImageFormat.Png));
        }
        /// <summary>
        /// Converts bitmap to ByteArray
        /// </summary>
        /// <param name="bmp">Bitmap to convert</param>
        /// <param name="type">Destination type</param>
        /// <returns>Bytes of converted bitmap</returns>
        public static byte[] BitmapToByteArray(Bitmap bmp, ImageFormat type)
        {
            byte[] imageBytes = null;
            using (var memStream = new MemoryStream())
            {
                bmp.Save(memStream, type);
                imageBytes = memStream.ToArray();

            }
            return imageBytes;
        }
        /// <summary>
        /// Converts bitmap to pdf
        /// </summary>
        /// <param name="bmp">Bitmap to convert</param>
        /// <returns>The bytes of bitmap as pdf</returns>
        public static byte[] ImageToPdf(Bitmap bmp)
        {
            Document document = new Document();
            using (var stream = new MemoryStream())
            {
                PdfWriter.GetInstance(document, stream);
                document.Open();
                var bytes = BitmapToByteArray(bmp, ImageFormat.Png);
                var image = iTextSharp.text.Image.GetInstance(bytes);
                document.Add(image);
                document.Close();
                return stream.ToArray();
            }

        }
    }
}
