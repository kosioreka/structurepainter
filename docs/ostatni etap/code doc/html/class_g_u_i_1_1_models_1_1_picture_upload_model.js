var class_g_u_i_1_1_models_1_1_picture_upload_model =
[
    [ "PictureUploadModel", "class_g_u_i_1_1_models_1_1_picture_upload_model.html#aaf073489adb4aa6f84d0072123e36df7", null ],
    [ "PictureUploadModel", "class_g_u_i_1_1_models_1_1_picture_upload_model.html#a3d4a88a62c4ac8d61b2bbb181f9fc8b2", null ],
    [ "GetImageBytes", "class_g_u_i_1_1_models_1_1_picture_upload_model.html#a6eccf3d38a15e7b212568ee7f0831e19", null ],
    [ "FileName", "class_g_u_i_1_1_models_1_1_picture_upload_model.html#a25f7dd12bde12e2de5bae1bc07ae5305", null ],
    [ "ImageData", "class_g_u_i_1_1_models_1_1_picture_upload_model.html#a5cc5d979d8bca0b77b5fe950c4255954", null ],
    [ "Name", "class_g_u_i_1_1_models_1_1_picture_upload_model.html#ae9fe03db676d070a8ac9710037305d12", null ]
];