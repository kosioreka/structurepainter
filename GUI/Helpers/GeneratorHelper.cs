﻿using GUI.Models;
using Model;
using Shared;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Web;
using System.Windows.Controls;
using System.Windows.Media;
using VisualizerProject;
using Color = System.Drawing.Color;
using Brushes = System.Windows.Media.Brushes;
using System.IO;

namespace GUI.Helpers
{
    /// <summary>
    /// Helper for structure generator
    /// </summary>
    public class GeneratorHelper
    {
        /// <summary>
        /// Exception used by drawing thread
        /// </summary>
        private Exception _ThreadException = null;

        /// <summary>
        /// Parsing Structure
        /// </summary>
        /// <param name="model">Structure model</param>
        /// <returns></returns>
        public ParserObject ParseStructure(StructureGeneratorModel model)
        {
            var images = CreateImageDictionary(model.Description, model.PictureUploads);
            Parser.Parser parser = new Parser.Parser(images.Count > 0 ? images : null);
            ParserObject po = parser.ParseText(model.Description);
            return po;
        }
        /// <summary>
        /// Generates structure as image from the given model
        /// </summary>
        /// <param name="model">Structure model</param>
        /// <returns>Structure on bitmap</returns>
        public string GeneratePdfStructure(StructureGeneratorModel model)
        {
            if (model.Description != null)
            {
                Styles styles = SetStyles(model.Styles);
                ParserObject po = ParseStructure(model);
                string path = null;
                Thread thread = new Thread(() =>
                {
                    try
                    {
                        SimpleVisualizer sv = new SimpleVisualizer(styles);
                        Canvas can = sv.Visualize(po);
                        if (can != null)
                        {
                            can.Background = System.Windows.Media.Brushes.White;
                            path = CanvasConverters.SaveCanvasAsPdf(can);
                        }
                    }
                    catch (Exception e)
                    {
                        path = null;
                        _ThreadException = e;
                    }
                });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                thread.Join();
                if (_ThreadException != null)
                {
                    throw new Exception(_ThreadException.Message);
                }
                return path;
            }
            else
            {
                throw new Exception("Nic nie wpisałeś do pola tekstowego. :)");
            }
        }
        /// <summary>
        /// Generates structure as image from the given model
        /// </summary>
        /// <param name="model">Structure model</param>
        /// <returns>Structure on bitmap</returns>
        public Bitmap GenerateImageStructure(StructureGeneratorModel model)
        {
            if (model.Description != null)
            {
                Styles styles = SetStyles(model.Styles);
                ParserObject po = ParseStructure(model);
                Bitmap bmp = null;
                Thread thread = new Thread(() =>
                {
                    Canvas can;
                    try
                    {
                        SimpleVisualizer sv = new SimpleVisualizer(styles);
                        can = sv.Visualize(po);
                        if (can != null)
                        {
                            can.Background = System.Windows.Media.Brushes.White;
                            bmp = CanvasConverters.SaveCanvasAsBitmap(can);
                        }
                    }
                    catch (Exception e)
                    {
                        can = null;
                        _ThreadException = e;
                    }
                });
                thread.SetApartmentState(ApartmentState.STA);
                thread.Start();
                thread.Join();
                if (_ThreadException != null)
                {
                    throw new Exception(_ThreadException.Message);
                }
                return bmp;
            }
            else
            {
                throw new Exception("Nic nie wpisałeś do pola tekstowego. :)");
            }
        }
        /// <summary>
        /// Set styles for visuzalization from the given model
        /// </summary>
        /// <param name="stylesModel">Style model</param>
        /// <returns>Style property</returns>
        private Styles SetStyles(StyleModel stylesModel)
        {
            return new Styles(stylesModel.Font,
                ConvertToBrush(stylesModel.LineColor),
                ConvertToMediaColor(stylesModel.FontColor),
                stylesModel.Layout == Layout.Vertical,
                stylesModel.ArrayLayout == Layout.Vertical,
                stylesModel.Size,
                stylesModel.OriginalImageSize
                );

        }
        /// <summary>
        /// Converts color to brush 
        /// </summary>
        /// <param name="color">Color</param>
        /// <returns>Brush</returns>
        private SolidColorBrush ConvertToBrush(Color color)
        {
            if (color.Equals(Color.Red))
            {
                return Brushes.Red;
            }
            else if (color.Equals(Color.Yellow))
            {
                return Brushes.Yellow;
            }
            else if (color.Equals(Color.Brown))
            {
                return Brushes.Brown;
            }
            else if (color.Equals(Color.Green))
            {
                return Brushes.Green;
            }
            else if (color.Equals(Color.Blue))
            {
                return Brushes.Blue;
            }
            else if (color.Equals(Color.Gray))
            {
                return Brushes.Gray;
            }
            return Brushes.Black;
        }
        /// <summary>
        /// Converts color to media color 
        /// </summary>
        /// <param name="color">Color</param>
        /// <returns>Media Color</returns>
        private System.Windows.Media.Color ConvertToMediaColor(Color color)
        {
            if (color.Equals(Color.Red))
            {
                return Colors.Red;
            }
            else if (color.Equals(Color.Yellow))
            {
                return Colors.Yellow;
            }
            else if (color.Equals(Color.Brown))
            {
                return Colors.Brown;
            }
            else if (color.Equals(Color.Green))
            {
                return Colors.Green;
            }
            else if (color.Equals(Color.Blue))
            {
                return Colors.Blue;
            }
            else if (color.Equals(Color.Gray))
            {
                return Colors.Gray;
            }
            return Colors.Black;
        }

        /// <summary>
        /// Creates dictionary of uplad pictures
        /// </summary>
        /// <param name="description">Structure description</param>
        /// <param name="uploads">Picture uploads model</param>
        /// <returns>Pictures</returns>
        private Dictionary<string, Bitmap> CreateImageDictionary(string description,
            List<PictureUploadModel> uploads)
        {
            var images = new Dictionary<string, Bitmap>();
            //var indexes = ImageConverters.AllIndexesOf(description, "{");
            var duplicates = uploads.GroupBy(p => p.Name)
                .Where(group => group.Count() > 1)
                .Select(group => group.Key);
            if (duplicates.Count() != 0)
            {
                throw new ArgumentException(ErrorType.DuplicatedPicture.ToFriendlyString(duplicates.First()));
            }
            foreach (var img in uploads)
            {
                images.Add(img.Name, ImageConverters.Base64StringToBitmap(img.GetImageBytes()));
            }
            return images;

            //this error handing should be done in parser!

            //if (description.Contains("graph") || description.Contains("tree"))
            //{
            //    indexes.RemoveAt(0); //if graph is defined
            //}
            //foreach (var index in indexes)
            //{
            //    string img = description.Substring(index + 1, description.IndexOf('}', index + 1) - index - 1);
            //    if (uploads != null)
            //    {
            //        var pic = uploads.FirstOrDefault(p => p.Name == img || p.FileName == img);
            //        if (pic != null)
            //        {
            //            images.Add(img, ImageConverters.Base64StringToBitmap(pic.GetImageBytes()));
            //        }
            //        else
            //        {
            //throw new ArgumentException(ErrorType.PictureNotFound.ToString(), img);
            //        }
            //    }
            //    else
            //    {
            //        throw new ArgumentException(ErrorType.PictureNotFound.ToString(), img);
            //    }
            //}
        }
    }
}