﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GUI.Controllers;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Net.Http;
using System.Web;
using GUI.Models;
using GUI.Helpers;

namespace ModuleTests
{
    [TestClass]
    public class GUITests
    {
        [TestMethod]
        public void ReturnsViewDataTest()
        {
            StructureGeneratorController controller = new StructureGeneratorController();
            var result = controller.Index() as ViewResult;
            Assert.IsInstanceOfType(result.ViewData["FontList"], typeof(List<SelectListItem>));
            Assert.IsInstanceOfType(result.ViewData["ColorList"], typeof(List<SelectListItem>));
        }
        [TestMethod]
        public void GeneratesStructure()
        {
            StructureGeneratorController controller = new StructureGeneratorController();
            var result = controller.Index() as ViewResult;
            var model2 = result.Model as StructureGeneratorModel;
            model2.Description = "[Ala | ma | kota]";

            var jsonResult = controller.Generate(model2) as JsonResult;
            dynamic resultData = new JsonResultDynamicWrapper(jsonResult);
            Assert.IsNotNull(jsonResult.Data);
            Assert.AreEqual(false, resultData.isError);
            Assert.IsInstanceOfType(resultData.value, typeof(string));
        }

        [TestMethod]
        public void GeneratesStructureError()
        {
            StructureGeneratorController controller = new StructureGeneratorController();
            var result = controller.Index() as ViewResult;
            var model2 = result.Model as StructureGeneratorModel;
            model2.Description = "[{Ala | ma | kota]]";

            var jsonResult = controller.Generate(model2) as JsonResult;
            dynamic resultData = new JsonResultDynamicWrapper(jsonResult);
            Assert.IsNotNull(jsonResult.Data);
            Assert.AreEqual(true, resultData.isError);
            Assert.IsNotNull(resultData.Error);
        }
    }
}
