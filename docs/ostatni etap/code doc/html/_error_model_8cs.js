var _error_model_8cs =
[
    [ "ErrorType", "_error_model_8cs.html#ab921414f6c4e52c8db2a7760f1f117ca", [
      [ "PictureNotFound", "_error_model_8cs.html#ab921414f6c4e52c8db2a7760f1f117caab6576e5eba5f580f4120a78a34d8d4d7", null ],
      [ "SyntaxError", "_error_model_8cs.html#ab921414f6c4e52c8db2a7760f1f117caaece592fd242f4ba674cc9da539a625a7", null ],
      [ "DuplicatedPicture", "_error_model_8cs.html#ab921414f6c4e52c8db2a7760f1f117caad454bb1a3eb27585c12a6cd63d129134", null ],
      [ "Unknown", "_error_model_8cs.html#ab921414f6c4e52c8db2a7760f1f117caa88183b946cc5f0e8c96b2e66e1c74a7e", null ]
    ] ]
];