﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUI.Models
{
    /// <summary>
    /// Picture upload model
    /// </summary>
    public class PictureUploadModel
    {
        /// <summary>
        /// Name of the picture
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// File name of the picture
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Image string data
        /// </summary>
        public string ImageData { get; set; }
        /// <summary>
        /// Converts from Base 64 String to bytes
        /// </summary>
        /// <returns></returns>
        public byte[] GetImageBytes()
        {
            return Convert.FromBase64String(ImageData);
        }

        public PictureUploadModel(string name, string fileName, string imageData)
        {
            Name = name;
            FileName = fileName;
            ImageData = imageData;
        }

        public PictureUploadModel()
        {
        }
    }
}