﻿using System;
using System.Windows;
using System.Windows.Media;

namespace Model
{
    /// <summary>
    /// Class containing visualization styles
    /// </summary>
    public class Styles
    {
        public Styles()
        {
            Size = StructureSize.Normal;
            CalculateStyles();
        }
        public Styles(string font, SolidColorBrush lineColor, Color fontColor, 
            bool verticalFlow, bool verticalArray, StructureSize size, bool orgImgSize)
        {
            Font = new FontFamily(font);
            LineColor = lineColor;
            FontColor = fontColor;
            VerticalFlow = verticalFlow;
            VerticalArray = verticalArray;
            Size = size;
            OriginalImageSize = orgImgSize;
            CalculateStyles();
        } 
        /// <summary>
        /// Calculates style values
        /// </summary>
        protected void CalculateStyles()
        {
            ArrowLength = StrokeSize * 5;
            NodeMarginWidth = 3 * Margin + StrokeSize;
            NodeMarginHeight = 2 * Margin + StrokeSize;
        }
        /// <summary>
        /// Color of text
        /// </summary>
        public Color FontColor { get; protected set; } = Colors.Black;
        /// <summary>
        /// Text font family
        /// </summary>
        public FontFamily Font { get; protected set; } = new FontFamily("Arial");
        /// <summary>
        /// Preferred font size
        /// </summary>
        public int FontSize { get; protected set; } = 16;
        /// <summary>
        /// Color of elements to draw
        /// </summary>
        public SolidColorBrush LineColor { get; protected set; } = Brushes.Crimson;
        /// <summary>
        /// Maximum size available
        /// </summary>
        public Size MaxSize { get; protected set; } = new Size(100, 100);
        /// <summary>
        /// Width of line
        /// </summary>
        public int StrokeSize { get; protected set; } = 2;
        /// <summary>
        /// Margin between drawn parts
        /// </summary>
        public int Margin { get; protected set; } = 20;
        /// <summary>
        /// Horizontal margin used in nodes of graphs
        /// </summary>
        public int NodeMarginWidth { get; protected set; }
        /// <summary>
        /// Vertical margin used in nodes of graphs
        /// </summary>
        public int NodeMarginHeight { get; protected set; }
        /// <summary>
        /// Length of arrow at the end of vector
        /// </summary>
        public int ArrowLength { get; protected set; }
        /// <summary>
        /// Angle at which to draw arrow
        /// </summary>
        public double ArrowAngle { get; protected set; } = Math.PI / 4;
        /// <summary>
        /// The distance between object and vector horizontal part
        /// </summary>
        public double ArrowHeight { get; protected set; } = 20;
        /// <summary>
        /// The distance between vector and it's label
        /// </summary>
        public double LabelMargin { get; protected set; } = 5;
        /// <summary>
        /// If true the next object is below previous, if false it is on the right
        /// </summary>
        public bool VerticalFlow { get; protected set; } = false;
        /// <summary>
        /// If true elements of the arrays are drawn from vertically, if false horizonatally
        /// </summary>
        public bool VerticalArray { get; protected set; } = true;
        /// <summary>
        /// If true images are not scaled
        /// </summary>
        public bool OriginalImageSize { get; protected set; } = false;
        /// <summary>
        /// Graph layout used by the graphviz renderer
        /// </summary>
        public static string GraphLayout { get; protected set; } = "dot";
        /// <summary>
        /// Setting structure style properties according to the size
        /// </summary>
        public StructureSize Size
        {
            set
            {
                switch (value)
                {
                    case StructureSize.Small:
                        FontSize = 12;
                        StrokeSize = 1;
                        Margin = 15;
                        MaxSize = new Size(80, 80);
                        break;
                    case StructureSize.Normal:
                        FontSize = 16;
                        StrokeSize = 2;
                        Margin = 20;
                        MaxSize = new Size(100, 100);
                        break;
                    case StructureSize.Large:
                        FontSize = 20;
                        StrokeSize = 2;
                        Margin = 25;
                        MaxSize = new Size(120, 120);
                        break;
                    case StructureSize.Huge:
                        FontSize = 25;
                        StrokeSize = 3;
                        Margin = 30;
                        MaxSize = new Size(140, 140);
                        break;
                }
                ArrowHeight = Margin;
            }
        }
    }
    /// <summary>
    /// The enumeration of possible sizes
    /// </summary>
    public enum StructureSize { Small, Normal, Large, Huge }

}
