﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GUI.Models
{
    /// <summary>
    /// Structure generator model
    /// </summary>
    public class StructureGeneratorModel
    {
        /// <summary>
        /// Text description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Picture uploads
        /// </summary>
        public List<PictureUploadModel> PictureUploads { get; set; }
        /// <summary>
        /// Style of the structure
        /// </summary>
        public StyleModel Styles { get; set; }
        /// <summary>
        /// Download format
        /// </summary>
        public DownloadType DownloadFormat { get; set; }
        //public List<ErrorModel> Errors { get; set; }

        public StructureGeneratorModel()
        {
            Styles = new StyleModel()
            {
                Size = Model.StructureSize.Normal,
                LineColor = System.Drawing.Color.Red,
                Font = "Arial"
            };
            DownloadFormat = DownloadType.PNG;
            PictureUploads = new List<PictureUploadModel>();
        }
        public StructureGeneratorModel(string description): base()
        {
            Description = description;
        }
    }

}