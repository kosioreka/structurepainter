var class_model_1_1_styles =
[
    [ "Styles", "class_model_1_1_styles.html#a0d3e4215d3ecb12c756eb7dd9995a89a", null ],
    [ "Styles", "class_model_1_1_styles.html#adedb129ecd7caa7250e964b15e61de1d", null ],
    [ "CalculateStyles", "class_model_1_1_styles.html#a130233d8312f008ff4b1f08aa2067fa6", null ],
    [ "ArrowAngle", "class_model_1_1_styles.html#a297929d9f63bba8f8f4385aeef6d2619", null ],
    [ "ArrowHeight", "class_model_1_1_styles.html#a78b534851825088eb4ad95491ac67fe6", null ],
    [ "ArrowLength", "class_model_1_1_styles.html#a242ba4581e7de796154e57f134713601", null ],
    [ "Font", "class_model_1_1_styles.html#a2bf9b871600f281bcf3c24241d6ae411", null ],
    [ "FontColor", "class_model_1_1_styles.html#a7a249b7b412ed9a256d4255817864b40", null ],
    [ "FontSize", "class_model_1_1_styles.html#aae4188cb0c618d849e74ad3641adb150", null ],
    [ "LabelMargin", "class_model_1_1_styles.html#a5ec450d45f4f3414016d83f24692d779", null ],
    [ "LineColor", "class_model_1_1_styles.html#a29f37510873e0885efed525ed3aa1a4d", null ],
    [ "Margin", "class_model_1_1_styles.html#a493c5a4f54363a815118f909c9e1b45c", null ],
    [ "MaxSize", "class_model_1_1_styles.html#a036683c9e4fb850578dfd0183af75c43", null ],
    [ "NodeMarginHeight", "class_model_1_1_styles.html#a52053ba374c33b73e6822c4c34464abb", null ],
    [ "NodeMarginWidth", "class_model_1_1_styles.html#affccb560e3a8f78a5936446c15aeaa70", null ],
    [ "OriginalImageSize", "class_model_1_1_styles.html#adb283bab7f71dcfc9df1e34e8d104327", null ],
    [ "Size", "class_model_1_1_styles.html#a8c032e0939afbcd8ae89ff01e47646e4", null ],
    [ "StrokeSize", "class_model_1_1_styles.html#a0a830055aaf12ebfbc78f87b1ef3926e", null ],
    [ "VerticalArray", "class_model_1_1_styles.html#ac444d8b3f144354bae345fe9cd8aa52a", null ],
    [ "VerticalFlow", "class_model_1_1_styles.html#af61196784a316186b65bd799dbe50db5", null ]
];