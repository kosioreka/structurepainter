﻿using System.Windows.Controls;
using Model;


namespace VisualizerProject
{
    /// <summary>
    /// Base class for all visualizers
    /// </summary>
    public abstract class Visualizer
    {
        /// <summary>
        /// The style of generated visualization
        /// </summary>
        protected Styles Styles
        {
            get; set;
        }
        /// <summary>
        /// Visualization helper
        /// </summary>
        protected VisualizerHelper Helper { get; set; }
        public abstract Canvas Visualize(ParserObject po);

    }
}
